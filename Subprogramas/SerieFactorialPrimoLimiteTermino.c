/**
 * @file SerieFactorialPrimoNterminos.c
 * @author your name (you@domain.com)
 * @brief Generar la serie y la sumatoria en base al límite nTerminos
 * S = 2! + 3! + 5! + 7! + 11! + 13! + .......
 * @version 0.1
 * @date 2024-01-17
 * 
 * @copyright Copyright (c) 2024
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int solicitarLimiteTerminos();
double generarTerminos(int nTerminos);
bool verificaNumeroPrimo(int numero);
double calcularFactorial(int numero);

int generarSigTermino(int terminoActual);

int main(int argc, char const *argv[]){
    system("@cls||clear");
    int limiteTerminos = solicitarLimiteTerminos();
	double suma = generarTerminos(limiteTerminos);
	//presentarSumatoria(suma);
    printf("\nS = %F\n", suma);
    printf("Gracias por usar nuestro programa!!\n");
    getchar();
    return 0;
}

bool verificaNumeroPrimo(int numero){
    bool esPrimo = numero > 1;
    for (int i = 2; i < numero; i++){
        if (numero % i == 0){
            esPrimo = false;
            break;
        }
    }
    return esPrimo;
}

/*
double generarTerminos(int nTerminos){
    int terminoPrimo = 2;
    double sumatoria = 0;
    printf("S = ");
    while (terminoPrimo <= nTerminos){
        bool esPrimo = verificaNumeroPrimo(terminoPrimo);
        if (esPrimo){
            printf("%d! + ", terminoPrimo);
            double factorial = calcularFactorial(terminoPrimo);
            sumatoria = sumatoria + factorial;
        }
        terminoPrimo++;
    }    
    return sumatoria;
}
*/

double generarTerminos(int nTerminos){
    int terminoPrimo = 1;
    double sumatoria = 0;
    printf("S = ");
    while (terminoPrimo <= nTerminos){
        terminoPrimo = generarSigTermino(terminoPrimo);
        if (terminoPrimo > nTerminos){
            break;
        }
        printf("%d! + ", terminoPrimo);
        double factorial = calcularFactorial(terminoPrimo);
        sumatoria = sumatoria + factorial;
    }    
    return sumatoria;
}

int generarSigTermino(int terminoActual){
    /*
    terminoActual++;
    while (!verificaNumeroPrimo(terminoActual)){
        terminoActual++;
    } 
    */
    bool esPrimo = false;
    do{
        terminoActual++;
        esPrimo = verificaNumeroPrimo(terminoActual);
    } while (!esPrimo);
    return terminoActual;
}

double calcularFactorial(int numero){
    double factorial = 1.0;
    for ( ; numero >  1; numero--){
        factorial = factorial * numero;
    }
    return factorial;    
}

int solicitarLimiteTerminos(){
    int nTerminos;
    printf("Ingrese el límite de terminos de la serie: ");
    scanf("%d",&nTerminos);
    getchar();
    return nTerminos;
}