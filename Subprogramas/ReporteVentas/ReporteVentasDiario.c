/**
 * @file ReporteVentasDiario.c
 * @author PRIMERO CC (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2024-01-19
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#include <stdio.h>
#include <stdlib.h>

struct ItemReporteVenta{
    char *descripcion;
    int cantidad;
    float monto;
};


// API's
int ingresaNumeroVentas();
struct ItemReporteVenta inicializarItemReporteVenta(char descripcion[]);
// Por referencia
void ingresarVentasDiarias(int numeroVentas, struct ItemReporteVenta *A, struct ItemReporteVenta *B, struct ItemReporteVenta *C);
// Por referencia
void clasificarVenta(float montoVenta, struct ItemReporteVenta *A, struct ItemReporteVenta *B, struct ItemReporteVenta *C);
struct ItemReporteVenta totalizarVenta(struct ItemReporteVenta A, struct ItemReporteVenta B, struct ItemReporteVenta C);
void presentReporte(struct ItemReporteVenta A, struct ItemReporteVenta B, struct ItemReporteVenta C, struct ItemReporteVenta total);


int main(int argc, char const *argv[]){
    system("@cls||clear");
    
    int numeroVentasAlDia;
    struct ItemReporteVenta A, B, C;
   
    A = inicializarItemReporteVenta("Ventas > 1000");
    B = inicializarItemReporteVenta("Ventas 500 ... 1000");
    C = inicializarItemReporteVenta("Ventas < 500");
    
    
    /*
    clasificarVenta(1500.8, &A, &B, &C);
    clasificarVenta(503.68, &A, &B, &C);
    clasificarVenta(500.75, &A, &B, &C);
    clasificarVenta(100, &A, &B, &C);
    
    printf("Venta Incializada A: \n");
    printf("A.descripcion : %s\n", A.descripcion);
    printf("A.cantidad : %i\n", A.cantidad);
    printf("A.monto : %f\n", A.monto);
    printf("Venta Incializada B: \n");
    printf("B.descripcion : %s\n", B.descripcion);
    printf("B.cantidad : %i\n", B.cantidad);
    printf("B.monto : %f\n", B.monto);
    printf("Venta Incializada C: \n");
    printf("C.descripcion : %s\n", C.descripcion);
    printf("C.cantidad : %i\n", C.cantidad);
    printf("C.monto : %f\n", C.monto);
    */
    
    return 0;
}

void clasificarVenta(float montoVenta, struct ItemReporteVenta *A, struct ItemReporteVenta *B, struct ItemReporteVenta *C){
    if (montoVenta > 1000){
        A->monto = A->monto + montoVenta;
        //(*A).monto = (*A).monto + montoVenta;
        A->cantidad++;
    } else {
        if (montoVenta > 500 && montoVenta <= 1000){
            B->monto = B->monto + montoVenta;
            B->cantidad++;
        } else {
            C->monto = C->monto + montoVenta;
            C->cantidad++;
        }
    }
}


struct ItemReporteVenta inicializarItemReporteVenta(char *descripcion){
    struct ItemReporteVenta itemReporteVenta;
    itemReporteVenta.descripcion = descripcion;
    itemReporteVenta.cantidad = 0;
    itemReporteVenta.monto = 0;
    return itemReporteVenta;
}