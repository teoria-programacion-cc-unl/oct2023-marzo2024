Un empleado de la tienda “Tiki Taka” realiza N ventas durante el día, se re-
quiere saber cuántas de ellas fueron mayores a $1000, cuántas fueron ma-
yores a $500 pero menores o iguales a $1000, y cuántas fueron menores o
iguales a $500. Además, se requiere saber el monto de lo vendido en cada
categoría y de forma global.

ANÁLISIS Y DETERMINACIÓN DE REQUERIMIENTOS
============================================

DATOS ENTRADA				DATOS SALIDA
-----------------------------------------------------
- numeroVentas (en el día)		Reporte Ventas:
- valorVenta (de cada del día)			ItemReporteVentas A,B,C, TotalVenta





Reporte Ventas
----------------	

Fecha: (yyyy-mm-dd)
	Categoría		 cantidad		monto
(A)  Ventas > 1000			5		5000
(B)  Ventas Entre 500..1000		8		4000
(C)  Ventas < 500		 	20		3500
-----------------------------------------------------------------
     TOTAL Venta			33		12500



ItemReporteVenta {
	- Texto descripcion
	- Entero cantidad;
	- Real monto
} 


ALGORITMO (InformeVentasDiarios)

ItemReporteVenta {
	- Texto descripcion
	- Entero cantidad;
	- Real monto
}

VARIABLES
	Entero numeroVentasAlDia;
	ItemReporteVenta A, ItemReporteVenta B, ItemReporteVenta C, ItemReporteVenta, total;

INICIO
	numeroVentasAlDia = ingresaNumeroVentas();
	A = inicializarItemReporteVenta("Ventas > 1000");
	B = inicializarItemReporteVenta("Ventas Entre 500..1000");
	C = inicializarItemReporteVenta("Ventas < 500");
		
	ingresarVentasDiarias(numeroVentas, Ref (A,B,C));
	total = totalizarVenta(A,B,C);
	presentarReporte(A,B,C,total)
FIN


FUNCION inicializarItemReporteVenta(Texto descripcion) RETURN ItemReporteVenta
	ItemReporteVenta itemReporteVenta;
	itemReporteVenta.descripcion = descripcion;
	itemReporteVenta.cantidad = 0;
	itemReporteVenta.monto = 0;
	RETURN itemReporteVenta;
FIN_FUNCION


PROCEDIMIENTO ingresarVentasDiarias (Entero numeroVentas, 
		Ref (ItemReporteVenta A, ItemReporteVenta B, ItemReporteVenta C))
	Real valorVenta
	Entero i;
	PARA (i = 1, i <= numeroVentas; i++) HACER 
		ESCRIBIR ("Ingrese el monto de la venta: ");
		LEER (valorVenta);
		clasificarVenta(valorVenta, ItemReporteVenta A, ItemReporteVenta B, ItemReporteVenta C);
	FIN_PARA
FIN_PROCEDIMENTO

PROCEDIMEITO clasificarVenta(Entero montoVenta, 
		REF Ref (ItemReporteVenta A, ItemReporteVenta B, ItemReporteVenta C)
	SI (montoVenta > 1000) ENTONCES
		A.monto = A.monto + montoVenta;
		A.cantidad++;
	SINO
		SI (montoVenta > 500 && montoVenta <= 1000) ENTONCES
			B.monto = B.monto + montoVenta;
			B.cantidad++;
		SINO
			C.monto = C.monto + montoVenta;
			C.cantidad++;
		FIN_SI	
	FIN_SI
FIN_PROCEDIMENTO

FUNCION totalizarVenta(ItemReporteVenta A, ItemReporteVenta B, ItemReporteVenta C) RETORNAR ItemReporteVenta
	ItemReporteVenta total;
	total.descripcion = "Total Ventas"
	total.cantidad = A.cantidad + B.cantidad + C.cantidad;
	total.monto = A.monto + B.monto + C.monto;
	RETURN total;
FIN_FUNCION

PROCEDIMIENTO presentarReporte(ItemReporteVenta A, ItemReporteVenta B, ItemReporteVenta C, ItemReporteVenta total)
Reporte Ventas
----------------

Fecha: (yyyy-mm-dd)
	Categoría		 cantidad		monto
(A)  Ventas > 1000			A.cantidad	?
(B)  Ventas Entre 500..1000		?		?
(C)  Ventas < 500		 	?		?
-----------------------------------------------------------------
     TOTAL Venta			?		?
FIN


Abrir diagrama Actividades en https://demo.bpmn.io/
