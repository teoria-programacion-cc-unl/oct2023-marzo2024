/**
 * @file Factorial.c
 * @author Wilman Chamba Z (you@domain.com)
 * @brief Calcular el factoria de un numero natural 
 * @version 0.1
 * @date 2024-01-16
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#include <stdio.h>
#include <stdlib.h>

// DECLARANDO API --> IDENTIFANDO PROTITIPO DEL SUBPROGRAMA (PROTOCOLOS)
/**
 * @brief Retornar un número natural positivo ingresado por el usuario 
 * @return int 
 */

int ingresarNumero();
double calcularFactorial(int numero);
double calcularFactorialRef(int *numero);

int main(int argc, char const *argv[]){
    char opcion;
    do{
        system("@cls||clear");
        printf("FACTORIAL DE UN NUMERO\n");
        int numero = ingresarNumero();

        printf("******* Dirección de Memoria de numero MAIN %p\n", &numero);

        double factorial = calcularFactorial(numero);
        printf("%d! = %F\n", numero, factorial);

        factorial = calcularFactorialRef(&numero);
        printf("%d! = %F\n", numero, factorial);

        
        printf("Deseas continar con la ejecución, (S) para continuar,  cualquier carcter para salir? ");
        scanf("%c", &opcion);
        getchar();

    } while (opcion == 'S' || opcion == 's');
    printf("Gracias por usar nuestro programa!!");
    getchar();
    return 0;
}

/**
 * @brief Calcular el factorial de un número
 * 
 * @param numero 
 * @return double 
 */
double calcularFactorial(int numero){
    printf("******* \n");
    printf("******* \n");
    printf("******* Dirección de Memoria EN FUNCION CALCULAR: %p\n", &numero);
    printf("******* \n");
    printf("******* \n");

    double resultado = 1.0;
    while (numero > 1){
        resultado = resultado * numero;
        numero--;
    }
    return resultado;
}

double calcularFactorialRef(int *numero){
    printf("******* \n");
    printf("******* \n");
    printf("******* Dirección de Memoria EN FUNCION CALCULAR REFERENCIA: %p\n", numero);
    printf("******* \n");
    printf("******* \n");

    double resultado = 1.0;
    while (*numero > 1){
        resultado = resultado * (*numero);
        (*numero)--;
    }
    return resultado;
}

/**
 * @brief Ingresar números naturales positivos
 * 
 * @return int 
 */
int ingresarNumero(){
    int n;
    do{
        printf("Ingrese un número: ");
        scanf("%d",&n);
        getchar();
        if (n < 0) {
            printf("Valor incorrecto: (Valor >= 0)\n");
        }
    } while (n < 0);
    return n;   
}







