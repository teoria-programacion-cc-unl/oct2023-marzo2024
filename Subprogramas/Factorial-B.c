/**
 * @file Factorial-B.c
 * @author Wilman Chamba Z.
 * @brief  Calcular el factorial de números natutales positivos
 * @version 0.1
 * @date 2024-01-16
 * @copyright Copyright (c) 2024
 * 
 */

#include <stdio.h>
#include <stdlib.h>

// DECLARACIÓN DE FUNCION ---> PROTOTIPO DE FUNCIONES
// DEFINIMOS EL API : PROTOCOLOS DE COMUNICACION 
/**
 * @brief Retornar un número natural positivo ingresado por el usuario 
 * @return int 
 */
int solicitarNumero();
double calcularFactorial(int numero);
double calcularFactorialRef(int *numero);

void main(int argc, char const *argv[]){
    char opcion;
    do{
        system("@cls||clear");
        
        int numero = solicitarNumero();

        printf("*****DIRECCIÓN DE MEMORIA VARIABLE numero en el MAIN: %p\n", &numero);
        double resultado = calcularFactorial(numero);
        printf("%d! = %F\n", numero, resultado);

        resultado = calcularFactorialRef(&numero);
        printf("%d! = %F\n", numero, resultado);
        
        printf("Desea continuar con la ejecucion? [s] para salir, cualquier tecla para continuar... ");
        scanf("%c", &opcion);
        getchar();

    } while(opcion != 'S' && opcion != 's');
        
    printf("Gracias por usar nuestro programa, vuelve pronto!!");
    getchar();

}

/**
 * @brief Calcular el factorial de un número
 * 
 * @param numero 
 * @return double 
 */
double calcularFactorial(int numero){
    printf("*****\n");
    printf("*****\n");
    printf("*****DIRECCIÓN DE MEMORIA PARAMETRO numero en FUNCION (VALOR): %p\n", &numero);
    printf("*****\n");
    printf("*****\n");

    double factorial = 1.0;
    while (numero > 1){
        factorial = factorial * numero;
        numero--;
    }
    return factorial;
}

double calcularFactorialRef(int *numero){
    printf("*****\n");
    printf("*****\n");
    printf("*****DIRECCIÓN DE MEMORIA PARAMETRO numero en FUNCION (REF): %p\n", numero);
    printf("*****\n");
    printf("*****\n");

    double factorial = 1.0;
    while (*numero > 1){
        factorial = factorial * (*numero);
        (*numero)--;
    }
    return factorial;
}

int solicitarNumero(){
    int n;
    do{
        printf("Ingrese el número a calcular: ");
        scanf("%d",&n);
        getchar();
        if (n < 0){
            printf("Valor incorrecto, Vuelva ingresar un valor > 0\n");
        }
    } while (n < 0);
    return n;
}



