/**
 * @file TablaPitagorica.c
 * @author your name (you@domain.com)
 * @brief Tabla Pitagorica de Multiplicar 
 * @version 0.1
 * @date 2024-01-16
 * 
 * @copyright Copyright (c) 2024
 * 
 */

/**
 
 * Mostrar la tabla pitagórica hasta un límite

 *      X	1	2	3	4      
 *      1	1	2	3	4
 *      2	2	4	6	8
 *      3	3	6	9	12
 *      4	4	8	12	16


 * ingresarNumero(): intero
 * imprimirEncabezado(entero)
 * imprimirCuerpo(entero)
 */

#include<stdio.h>
#include<stdlib.h>

int ingresarNumeroTabla();
void imprimirEncabezado(int numero);
void imprimirCuerpo(int numero);

#define to_yellow "\033[33m"
#define to_bold "\033[1m"
#define to_default "\033[0m"


int main(int argc, char const *argv[]){
    system("@cls||clear");
    
    int numeroTabla = ingresarNumeroTabla();
    imprimirEncabezado(numeroTabla);
    imprimirCuerpo(numeroTabla);

    return 0;
}

int ingresarNumeroTabla(){
    int n;
    printf("Ingrese el número de la tabla que desee construir: ");
    scanf("%d", &n);
    if (n < 0){
        printf ("Valore incorrecto, debe ser positivo, trasformando a positivo");
        n = n*-1;
    }
    return n;
}

void imprimirEncabezado(int numero){
    printf("%s%s", to_bold, to_yellow);
    printf("\nTABLA PITAGORICA HASTA %d\n\n", numero);
    printf("X\t");
    printf("%s", to_default);
    for (int i = 1; i <= numero; i++){
        printf("%i\t", i);
    }
    printf("\n");
}

void imprimirCuerpo(int numero){
    for (int i = 1; i <= numero; i++){
        for (int j = 0; j <= numero; j++){
            if (j == 0){
                printf("%s", to_default);
                printf("%i\t", i);    
            } else {
                printf("%s", to_bold);
                printf("%i\t", i*j);
            }
        }
        printf("\n");
    }
}