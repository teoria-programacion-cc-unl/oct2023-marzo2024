/**
 * @file TablaPitagorica-B.c
 * @author Wilman Chamba Z.
 * @brief Generar la tabla pitagorica hasta un numero
 * @version 0.1
 * @date 2024-01-16
 * 
 * @copyright Copyright (c) 2024
 * 
 */

/**
 * @author: Wilman Chamba Z.
 * Mostrar la tabla pitagórica hasta un límite

 *       0  1   2   3   4

 *  0    X	1	2	3	4      
 *  1    1	1	2	3	4
 *  2    2	2	4	6	8
 *  3    3	3	6	9	12
 *  4    4	4	8	12	16
 * 
 *      solicitarLimite(): entero   ---> Camila;
 *      imprimirEncabezo(entero)    ---> Ariel
 *      imprimirCuerpo(entero)      ---> Wilman
 */

#include <stdio.h>
#include <stdlib.h>

#define to_yellow "\033[33m"
#define to_bold "\033[1m"
#define to_default "\033[0m"

int solicitarLimite();
void imprimirEncabezado(int limite);
void imprimirCuerpo(int limite);

int main(int argc, char const *argv[]){
    system("@cls||clear");
    int limite = solicitarLimite();
    imprimirEncabezado(limite);
    imprimirCuerpo(limite);

    return 0;
}

void imprimirCuerpo(int limite){
    // Recorrer filas
    for (int i = 1; i <= limite; i++){
        // Recorrer columnas de cada fila
        for (int j = 0; j <= limite; j++){
            if (j == 0){
                printf("%s", to_default);
                printf("%i\t", i);
            } else {
                printf("%s", to_bold);
                printf("%i\t", i*j);    
            }
        }
        printf("\n");
    }
}

void imprimirEncabezado(int limite){
    printf("%s%s", to_bold, to_yellow);
    printf("\n\n TABLA PITAGORICA HASTA %d\n\n", limite);
    
    printf("%s", to_default);
    printf("%s", to_yellow);
    printf("X\t");
    printf("%s", to_default);
    for (int i = 1; i <= limite; i++){
        printf("%i\t", i);
    }
    printf("\n");
}

int solicitarLimite(){
    int n;
    printf("Solicitar el límite tabla pitagorica: ");
    scanf("%d", &n);
    getchar();
    if (n < 0){
        printf("Valor incorrecto!!, se transformará a positivo\n");
        n = n * -1;
    }
    return n;
}


