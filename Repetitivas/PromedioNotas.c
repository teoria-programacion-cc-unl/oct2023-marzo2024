/**
 * @file PromedioNotas.c
 * @author wduck - Wilman Chamba
 * @brief Calcular el promedio de notas de una unidad 
 * @version 0.1
 * @date 2023-12-15
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include <stdio.h>
#include <stdlib.h>

#define ASIGNATURA "ELECTRICIDAD"
//#define NUM_ALUMNOS 5

struct Estudiante{
    char nombre[50];
    float calificacion;
    
};

struct Unidad
{
    char nombre[100];
    int numero;
};



int main(int argc, char const *argv[]){
    system("@cls||clear");
    struct Estudiante estudiante;
    struct Unidad unidad;
    float sumatoria = 0, promedio;
    int contador = 0;
    int numeroAlumos;
  
    printf("Calificaciones de cada estudiante de la Asignatura %s\n", ASIGNATURA);
    
    printf("Ingrese el número de la  unidad que va a promediar: ");
    scanf("%d", &unidad.numero); 
    getchar();
    printf("Ingrese el nombre de la  unidad que va a promediar: "); 
    //scanf("%s", unidad.nombre) --> solo ingreso de una sola palabra;
    fgets(unidad.nombre, 100, stdin);   
    getchar();

    printf("Cuantos estudiantes existen en la asignatura: ");
    scanf("%d", &numeroAlumos);
    getchar();

    while (contador < numeroAlumos){
        printf ("Ingresando información para el estudiante número [%d]:\n", contador);
        printf("Ingrese el nombre del estudiante: ");
        //scanf("%s", estudiante.nombre);
        scanf("%[^\n]", estudiante.nombre);
        getchar();
        printf("Ingrese la calificación obtenida en la unidad del estudiante: ");
        scanf("%f", &estudiante.calificacion);
        getchar();
        sumatoria = sumatoria + estudiante.calificacion;
        contador++; // contador = contador + 1;
    }
    
    promedio = sumatoria / numeroAlumos;
    
    printf("%s\n", ASIGNATURA);
    printf("Número de unidad: %d\n", unidad.numero);
    printf("Nombre de unidad: %s\n", unidad.nombre);
    printf("Promedio final: %.2f\n", promedio);

    return 0;
}
