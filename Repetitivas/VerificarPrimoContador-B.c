/**
 * @file VerificarPrimoContador-B.c
 * @author wduck (you@domain.com)
 * @brief Verificar un número primo a través de un contador
 * @version 0.1
 * @date 2024-01-09
 * 
 * @copyright Copyright (c) 2024
 * 
 */

/*
ALGORITMO VerificarNumeroPrimo
VARIABLES
	Entero numero, i, totalDivisores;
INICIO
	ESCRIBIR ("Ingrese número a verificar:");
	LEER (numero);
	i <- 1;
	totalDivisores <- 0;
	MIENTRAS (i <= numero) HACER
		SI (numero % i == 0) ENTONCES
			totalDivisores = totalDivisores+1;
		FINSI
		i++;
	FINMIENTRAS
	SI (totalDivisores == 2) ENTONCES
		ESCRIBIR (numero, "ES PRIMO");
	SINO
		ESCRIBIR (numero, "NO ES PRIMO");
	FINSI
FIN
*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[]){
    system("@cls||clear");

    int numero, totalDivisores;
    printf("Ingresar el núemro que desee verificar si es primo: ");
    scanf("%d", &numero);
    getchar();

    totalDivisores = 0;
    for (int i = 2; i < numero; i++){
        if (numero % i == 0){
            totalDivisores++;
            break;
        }
    }
    printf("Total divisores: %d\n", totalDivisores);
    if (numero > 1 && totalDivisores == 0){
        printf("%d ES PRIMO\n", numero);
    } else {
        printf("%d NO ES PRIMO\n", numero);
    }
    return 0;
}

