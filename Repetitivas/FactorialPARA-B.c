/**
 * @file FactorialPARA-B.c
 * @author Wilman Chamba Zaragocín
 * @brief Calcular el factorial de numeros naturales
 * @version 0.1
 * @date 2024-01-05
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#include <stdio.h>
#include <stdlib.h>


int main(int argc, char const *argv[]){
    
    char opcion;
    do{
        system("@cls||clear");
        int  n;
        long double factorial = 1;
        printf("FACTORIAL DE UN NUMERO\n");
        do{
            printf("Ingrese el número que desee calcular el factorial: ");
            scanf("%d",&n);
            getchar();
            if (n < 0){
                printf("Valor incorrecto, el número debe ser mayor o igual 0\n");
            }
        } while (n < 0);

        for (int i = n; i > 1; i--){
            factorial = factorial * i;
        }

        //printf("*** AHORA i vale %d\n", i);
        
        printf("%d! = %LF\n", n, factorial);
        printf("Desea continuar con la ejecucion? [s] para salir, cualquier tecla para continuar... ");
        scanf("%c", &opcion);
        getchar();

    } while(opcion != 'S' && opcion != 's');
    
    printf("Gracias por usar nuestro programa, vuelve pronto!!");
    getchar();

    return 0;
}
