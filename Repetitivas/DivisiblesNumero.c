/**
 * @file DivisiblesNumero.c
 * @author your name (you@domain.com)
 * @brief Indicar los números que le dividen exactamente a un número
 * @version 0.1
 * @date 2024-01-03
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[]){
    system("@cls||clear");
    int n, i, contadorDivisores = 0;
    
    printf("Ingrese el número a encontrar sus divisores: ");
    scanf("%d",&n);
    getchar();
    
    i = n;
    printf("Los números divisores son: \n");
    do{
        if (n%i == 0){
            printf("%d, ", i);
            contadorDivisores++;
        }
        i--; // i = i-1;
    } while ( i>0 );
    printf("\n");
    printf("Encontrados: %d divisores\n", contadorDivisores );
    printf("Gracias por usar nuestro programa!!");
    getchar();
    return 0;
}
