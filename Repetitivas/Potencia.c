/**
 * @file Potencia.c
 * @author Wilman Chamba Z (you@domain.com)
 * @brief Calcular la potencia basado en la base y el exponente, sin uso de librerías predefinidas
 * @version 0.1
 * @date 2024-01-12
 * 
 * @copyright Copyright (c) 2024
 * 
 */

/*
ALGORITMO CalcularPotencia
===========================

Potencia {
	Entero base, exponente;
}

VARIABLES
	Potencia potencia;
	Entero i;
	Real resultado = 1.0;
	Entero exponenteAux; 

INICIO
	ESCRIBIR ("Ingrese la base de la potencia: ");
	LEER (potencia.base);
	ESCRIBIR ("Ingrese el exponente de la potencia: ");
	LEER (potencia.exponente);
	
	exponenteAux = potencia.exponente;
	SI  (potencia.exponente < 0) ENTONCES
		exponenteAux = potencia.exponente * (-1);
	FIN_SI
	
	PARA (i = 1; i <= exponenteAux ; i++) HACER
		resultado = resultado * potencia.base;
	FIN_PARA
	
	SI  (potencia.exponente < 0) ENTONCES
		resultado = 1 / resultado;
	FIN_SI
	
	ESCRIBIR (potencia.base + "^" + potencia.exponente + "=" + resultado);
FIN
*/

#include <stdio.h>
#include <stdlib.h>

struct Potencia {
    int base, exponente;
};


int main(int argc, char const *argv[]){
    system("@cls||clear");
	
	struct Potencia potencia;
	int exponenteAux;
	double resultado = 1.0;

	printf("Ingrese la base de la potencia: ");
	scanf("%d", &potencia.base);
	getchar();
	printf ("Ingrese el exponente de la potencia: ");
	scanf("%d", &potencia.exponente);
	getchar();

	exponenteAux = potencia.exponente;
	if (potencia.exponente < 0){
		exponenteAux = potencia.exponente * -1;
	}

	for (int i = 1; i <= exponenteAux; i++){
		resultado = resultado * potencia.base;
	}
	
	if (potencia.exponente < 0){
		resultado = 1 / resultado;
	}

	printf("%d^(%d) = %lf\n", potencia.base, potencia.exponente, resultado);

    return 0;
}
