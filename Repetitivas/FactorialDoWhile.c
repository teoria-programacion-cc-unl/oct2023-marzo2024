/**
 * @file FactorialDoWhile.c
 * @author your name (you@domain.com)
 * @brief Calcular el factorial de un numeros positivos
 * @version 0.1
 * @date 2024-01-03
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[]){
    system("@cls||clear");
    int n, i;
    long double factorial = 1;
    
    printf("FACTORIAL DE UN NUMERO\n");
    do{
        printf("Ingrese el numero que desee calcular: ");
        scanf("%d", &n);
        getchar();
        if (n < 0){
            printf("Valor ingresado incorrecto\n");
        }        
    }while (n < 0);
    
    i = 1;
    do {
        factorial = factorial * i;
        i++;
    } while (i <= n);
    printf("%d! = %Lf\n", n, factorial);

    printf("Gracias por usar nuestro programa!!");
    getchar();

}