/**
 * @file VerificarPrimoPorBandera-B.c
 * @author Wilman Chamba Z.
 * @brief Verificar un numero primo utilizando bandera
 * @version 0.1
 * @date 2024-01-09
 * 
 * @copyright Copyright (c) 2024
 * 
 */
/*
ALGORITMO VerificarNumeroPrimoBandera
VARIABLES
	Entero numero, i; 
	Logico esPrimo;
INICIO
	ESCRIBIR ("Ingrese número a verificar:");
	LEER (numero);
	i = 2;
	esPrimo = numero > 1;
	 	
	MIENTRAS (esPrimo == VERDADERO && i < numero) HACER
		//esPrimo = (numero % i) == 0;
		SI (numero % i == 0) ENTONCES
			esPrimo = FALSO;
		FINSI
		i++;
	FINMIENTRAS
	SI (esPrimo == VERDADERO) ENTONCES   // SI (esPrimo) ENTONCES
		ESCRIBIR (numero, "ES PRIMO");
	SINO
		ESCRIBIR (numero, "NO ES PRIMO");
	FINSI
FIN
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

enum BOOLEAN { FALSE, TRUE};

int main(int argc, char const *argv[]){
    system("@cls||clear");

    int numero;
    bool esPrimo;
    //enum BOOLEAN esPrimo;  // Boolean: 0 FALSE, 1 TRUE
    //short esPrimo;  // Boolean: 0 FALSE, 1 TRUE

    printf("Ingrese el número que desee verifica si es primo: ");
    scanf("%d", &numero);
    getchar();

    esPrimo = numero > 1;
    /*
    if (numero > 1){
        esPrimo = 1;    
    } else {
        esPrimo = 0;
    }
    esPrimo = numero > 1 ? 1 : 0;
    */

   for (int i = 2; esPrimo == true && i < numero; i++){
        if (numero % i == 0){
            esPrimo = false;
        }
   }
   
   if (esPrimo){ // if (esPrimo == 1)
        printf("%d es PRIMO\n", numero);
   } else { 
        printf("%d NO es PRIMO\n", numero);
   }

    return 0;
}
