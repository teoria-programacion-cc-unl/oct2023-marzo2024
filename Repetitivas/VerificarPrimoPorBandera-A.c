/**
 * @file VerificarPrimoPorBandera-A.c
 * @author WDUCK
 * @brief Verificar si un numero ingresado es un primo o no, POR BANDERA
 * @version 0.1
 * @date 2024-01-09
 * 
 * @copyright Copyright (c) 2024
 * 
 */

/*
ALGORITMO VerificarNumeroPrimoBandera
VARIABLES
	Entero numero, i; 
	Logico esPrimo;
INICIO
	ESCRIBIR ("Ingrese número a verificar:");
	LEER (numero);
	i = 2;
	esPrimo = numero > 1;
	 	
	MIENTRAS (esPrimo == VERDADERO && i < numero) HACER
		//esPrimo = (numero % i) == 0;
		SI (numero % i == 0) ENTONCES
			esPrimo = FALSO;
		FINSI
		i++;
	FINMIENTRAS
	SI (esPrimo == VERDADERO) ENTONCES   // SI (esPrimo) ENTONCES
		ESCRIBIR (numero, "ES PRIMO");
	SINO
		ESCRIBIR (numero, "NO ES PRIMO");
	FINSI
FIN
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

enum BOOLEAN {FALSE, TRUE};

int main(int argc, char const *argv[]){
    system("@cls||clear");
    int numero;
    //short esPrimo;  // 0 FALSE , 1 TRUE
    //enum BOOLEAN esPrimo;
   bool esPrimo;

    printf("Ingrese el número primo a verificar? ");
    scanf("%d", &numero);
    getchar();

    esPrimo = numero > 1; 

    /*if (numero > 1){
         esPrimo = 1;
    } else {
        esPrimo = 0;
    }

    esPrimo = numero > 1 ?  1 : 0;
    */  
   
   for (int i = 2; esPrimo == 1 && i < numero; i++){
        //esPrimo = numero % i == 0;
        if( numero % i == 0){
            esPrimo = false;
        }
   } 

    if (esPrimo){ // if (esPrimo == 1)
        printf("%d ES PRIMO\n", numero);
    } else {
        printf("%d NO ES PRIMO\n", numero);
    }
    return 0;
}

















