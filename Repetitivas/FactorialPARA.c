/**
 * @file FactorialPARA.c
 * @author Wilman Chamba Z (you@domain.com)
 * @brief Calcular el factoria de un numero natural 
 * @version 0.1
 * @date 2024-01-04
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[]){
    char opcion;
    do{
        system("@cls||clear");

        int n;
        long double factorial;    
        printf("FACTORIAL DE UN NUMERO\n");
        do{
            printf("Ingrese el número que desea calcular el factorial:  ");
            scanf("%d", &n);
            getchar();
            if (n < 0){
                printf("Valor inocrrecto, se necesita un valor mayor a 0\n");
            }
        }while(n < 0);

        factorial = 1;
        for (int i = n; i > 1; i--){
            factorial = factorial * i;
        }
        //printf("AHORA I vale %d\n", i);

        printf("%d! = %LF\n", n, factorial);
        printf("Deseas continar con la ejecución, (S) para continuar,  cualquier carcter para salir? ");
        scanf("%c", &opcion);
        getchar();

    } while (opcion == 'S' || opcion == 's');
    printf("Gracias por usar nuestro programa!!");
    getchar();
    
    return 0;
}

