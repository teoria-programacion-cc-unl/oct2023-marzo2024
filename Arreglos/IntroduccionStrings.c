/**
 * @file IntroduccionStrings.c
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2024-02-14
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


void presentarArregloCadena(int limite, char cadena[limite]);
void ingresarArregloCadena(int limite, char cadena[limite]);
void presentarArregloEntero(int limite, int enteros[limite]);

int main(int argc, char const *argv[]){
    char conjunto[] = {'H', 'o', 'l' , 'a', ' ', 'M',  'u' , 'n', 'd', 'o', '\0'}  ;
    char cadena[30] = "Hola Mundo";
    char *nomina;
    int numeros[10] = {0,1,2,3,4,5,6,7,8,9};
    
    printf("Cadena: %s\n", cadena);
    printf("Size String Cadena: %li\n", strlen(cadena));
  
    nomina =  malloc(50 * sizeof(char));

    // Ingreso de Cadenas
    printf("Ingreso un texto para conjunto: ");
    scanf("%[^\n]", nomina);
    getchar();
    printf("Conjunto: %s\n", nomina);
    printf("Size String Conjunto: %li\n", strlen(nomina));

    printf("Ingreso sus nombres completos: ");
    fgets(nomina, (50 * sizeof(char)), stdin);
    nomina[strlen(nomina)-1] = '\0';
    printf("Nomina: %s\n", nomina);
    puts(nomina);
    printf("Size Nomina: %li\n", strlen(nomina));

    char c;
    printf ("Caracter a buscar: ");
    scanf("%c", &c);
    char *subcadena = strchr(nomina, c);
    puts(subcadena);






    return 0;
}

void presentarArregloCadena(int limite, char cadena[limite]){
    for (int i = 0; i < limite; i++){
        printf("%c", cadena[i]);
    }
    printf("\n");
}

void presentarArregloEntero(int limite, int enteros[limite]){
    for (int i = 0; i < limite; i++){
        printf("%i,", enteros[i]);
    }
    printf("\n");
}

void ingresarArregloCadena(int limite, char cadena[limite]){
    for (int i = 0; i < limite; i++){
        scanf("%c", &cadena[i]);
        getchar();
    }
}