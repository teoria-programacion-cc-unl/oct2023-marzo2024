/**
 * @file CalculadoraFracciones.c
 * @author wduck
 * @brief Realizar una calculadora de fracciones
 * @version 0.1
 * @date 2024-01-30
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#include <stdio.h>
#include <stdlib.h>

#define MAX_ELEMENTOS 100

struct Fraccion{
    int numerador, denominador;
};

struct CalculadoraFracciones{
   struct Fraccion fracciones[MAX_ELEMENTOS];
   int limiteFracciones;
};


//struct Fraccion sumar(struct Fraccion fraccion1, struct Fraccion fraccion2);
//struct Fraccion restar(struct Fraccion fraccion1, struct Fraccion fraccion2);
struct Fraccion multiplicar(struct Fraccion fraccion1, struct Fraccion fraccion2);
//struct Fraccion dividir(struct Fraccion fraccion1, struct Fraccion fraccion2);
struct Fraccion simplificar(struct Fraccion fraccion);
void simplificarPorReferencia(struct Fraccion *fraccion);
float convertirPuntoFlotante(struct Fraccion fraccion);
struct Fraccion ingresarFraccion(char *mensaje);
char elegirOperacion();
struct Fraccion calcular(char operacion, struct CalculadoraFracciones calculadora);

void presentarFraccion(struct Fraccion fraccion);
void presentarResultado(char operacion, struct CalculadoraFracciones calculadora, struct Fraccion rta);
int ingresarNumeroFraccionesOperar();
struct CalculadoraFracciones ingresarFracciones(int limite);
struct Fraccion sumarCalculadora(struct CalculadoraFracciones calculadora);
struct Fraccion multiplicarCalculadora(struct CalculadoraFracciones calculadora);
struct Fraccion multiplicarCalculadora2(int limiteFracciones, struct Fraccion fracciones[limiteFracciones]);

int main(int argc, char const *argv[])
{
    system("@cls||clear");
    int limite = ingresarNumeroFraccionesOperar();

    struct CalculadoraFracciones calculadora = ingresarFracciones(limite);
    char opcion = elegirOperacion();
    struct Fraccion rta = calcular(opcion, calculadora);

    presentarResultado(opcion, calculadora, rta);
    
    return 0;
}

struct CalculadoraFracciones ingresarFracciones(int limite){
    struct CalculadoraFracciones calculadora;
    calculadora.limiteFracciones = limite;

    for (int i = 0; i < calculadora.limiteFracciones; i++){
        char mensaje[30];
        sprintf(mensaje, "Ingresar la fraccion Nro. %d", i);
        struct Fraccion f = ingresarFraccion(mensaje);
        calculadora.fracciones[i] = f;
    }
    return calculadora;
}

char elegirOperacion(){
    return '*';
}

int ingresarNumeroFraccionesOperar(){
    int limite;
    printf("Cuantas fracciones desea operar: ");
    scanf("%d", &limite);
    getchar();
    return limite;
}

void presentarResultado(char operacion, struct CalculadoraFracciones calculadora, struct Fraccion rta){
    printf("\n");
    for (int i = 0; i < calculadora.limiteFracciones; i++){
        presentarFraccion(calculadora.fracciones[i]);
        printf(" %c ", operacion);    
    }
    printf("\b\b\b");
    printf(" = ");
    presentarFraccion(rta);
    printf(" ===> ");
    printf("%f \n", convertirPuntoFlotante(rta));
}

struct Fraccion sumarCalculadora(struct CalculadoraFracciones calculadora){
    struct Fraccion rta = calculadora.fracciones[0];
    for (int i = 1; i < calculadora.limiteFracciones; i++){
        //rta = sumar(rta, calculadora.fracciones[i]);
    }
    return rta;
}

struct Fraccion multiplicar(struct Fraccion fraccion1, struct Fraccion fraccion2){
    struct Fraccion fraccionRta;
    fraccionRta.numerador = fraccion1.numerador * fraccion2.numerador;
    fraccionRta.denominador = fraccion1.denominador * fraccion2.denominador;
    simplificarPorReferencia(&fraccionRta);
    return fraccionRta;
}

struct Fraccion multiplicarCalculadora(struct CalculadoraFracciones calculadora){
    struct Fraccion rta = calculadora.fracciones[0];
    for (int i = 1; i < calculadora.limiteFracciones; i++){
        rta = multiplicar(rta, calculadora.fracciones[i]);
    }
    return rta;
}

struct Fraccion multiplicarCalculadora2(int limiteFracciones, struct Fraccion fracciones[limiteFracciones]){
    struct Fraccion rta = fracciones[0];
    for (int i = 1; i < limiteFracciones; i++){
        rta = multiplicar(rta, fracciones[i]);
    }
    return rta;
}


struct Fraccion calcular(char operacion, struct CalculadoraFracciones calculadora){
    struct Fraccion rta;
    switch (operacion){rta;
        case '+':
            rta = sumarCalculadora(calculadora);
            break;
        case '-':
            //rta = restarCalculador(calculadora);
            break;
        case '*':
            rta = multiplicarCalculadora(calculadora);
            break;
        case '/':
            //rta = dividirCalulador(calculadora);
            break;
        default:
            rta.numerador = 0;
            rta.denominador = 0;
            break;
    }   
    return rta;
}


struct Fraccion ingresarFraccion(char *mensaje){
    struct Fraccion fraccion;
    //printf("%s[forma # / #]: " ,mensaje);
    //scanf("%d/%d", &fraccion.numerador, &fraccion.denominador);
    printf("%s:\n", mensaje);
    printf("Numerador: ");
    scanf("%d", &fraccion.numerador);
    getchar();
    printf("Denominador: ");
    scanf("%d", &fraccion.denominador);
    getchar();
    return fraccion; 
}



struct Fraccion simplificar(struct Fraccion fraccion){
    if (fraccion.denominador == 0){
        fraccion.numerador = 0;
        return fraccion;
    }

    int menor = fraccion.numerador;
    if (menor > fraccion.denominador){
        menor = fraccion.denominador;
    }

    for (int i = menor; i > 1; i--){
        if (fraccion.numerador % i == 0 &&  fraccion.denominador % i == 0){
            fraccion.numerador  = fraccion.numerador / i;
            fraccion.denominador  = fraccion.denominador / i;
            break;
        }
    }
    return fraccion;
}

void simplificarPorReferencia(struct Fraccion *fraccion){
    struct Fraccion simplificada = simplificar(*fraccion);
    fraccion->numerador = simplificada.numerador;
    fraccion->denominador = simplificada.denominador;
}

float convertirPuntoFlotante(struct Fraccion fraccion){
    return (float)fraccion.numerador / (float)fraccion.denominador;
}

void presentarFraccion(struct Fraccion fraccion){
    printf("%d / %d", fraccion.numerador, fraccion.denominador);
}