/**
 * @file ExamenRecuperacion.c
 * @author wduck (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2024-03-14
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

void imprimirPropiedadPrograma();
int ingresarNumeroElementosArreglo();
void ingresarArreglo(int longitud, int array[longitud]);
void triplicarArreglo(int longitud, int array[longitud], int arrayfinal[longitud]);
int esPrimo(int numero); 
void presentarArreglo(char *mensaje, int longitud, int array[longitud]);


int main(int argc, char const *argv[]){
    /* code */
    system("@cls||clear");
    char opcion;
    do
    {
        int numeroElementos = ingresarNumeroElementosArreglo();

        int arrayInicial[numeroElementos];
        int arraySolucion[numeroElementos];

        ingresarArreglo(numeroElementos, arrayInicial);
        triplicarArreglo(numeroElementos, arrayInicial, arraySolucion);    
        presentarArreglo("Arreglo Original", numeroElementos, arrayInicial);
        presentarArreglo("Arreglo Solución", numeroElementos, arraySolucion);

        printf("Desea continuar con la ejecución: ? [Cualquier tecla para continar , S{s} para Salir]: ");
        scanf("%c", &opcion);
        getchar();

    } while (opcion != 'S' && opcion != 's');

    imprimirPropiedadPrograma();
    return 0;
}

void ingresarArreglo(int longitud, int array[longitud]){
    printf("Ingresar el arreglo de enteros: \n" );
    for (int i = 0; i < longitud; i++){
        printf("Ingreso elemento[%d]: ", i);
        scanf("%d", &array[i]);
        getchar();
    }
}

void triplicarArreglo(int longitud, int array[longitud], int arrayfinal[longitud]){
    for (int i = 0; i < longitud; i++){
        int okPrimo = esPrimo(array[i]);
        if (okPrimo) {
            arrayfinal[i] = array[i] * 3;
        } else {
            arrayfinal[i] = array[i];
        }
    }
}

int esPrimo(int numero){
    int ok = numero > 1;
    for (int i = 2; (i < numero) && (ok == 1); i++){
        if ((numero % i) == 0){
            ok = 0;
        }
    }
    return ok;
}

void presentarArreglo(char *mensaje, int longitud, int array[longitud]){
    printf("%s:\n",mensaje);
    printf("[");
    for (int i = 0; i < longitud; i++){
        printf("%d, ", array[i]);
    }
    printf("]\n");
}

int ingresarNumeroElementosArreglo(){
    int tamanio;
    printf("Ingresar la longitud de Arreglo: ");
    scanf("%d",&tamanio);
    getchar();
    return tamanio;
}

void imprimirPropiedadPrograma(){
    printf("\nDesarrollado por: Wilman Chamba\n");
    printf("Fecha: 2024-03-12\n");
    printf("Evaluación de Recuperación\n");

}