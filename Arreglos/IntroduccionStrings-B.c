/**
 * @file IntroduccionStrings-B.c
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2024-02-15
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void presentarArregloCaracteres(int limite, char string[limite]);
void ingresarArregloCaracteres(int limite, char string[limite]);

int main(int argc, char const *argv[]){
    char conjunto[] = {'H', 'o', 'l', 'a', ' ', 'M', 'u', 'n', 'd', 'o', '\0', };
    char cadena[30] = "Hola mundo!!";
    char cadena1[] = "Hola mundo!!";
    char *cadena2;
    cadena2 = malloc(50*sizeof(char));


    printf("Conjunto: %s\n", conjunto);
    printf("Length Conjunto: %ld\n", strlen(conjunto));
    printf("\n");

    printf("Cadena: %s\n", cadena);
    printf("Length Cadena: %ld\n", strlen(cadena));
    puts(cadena);
    printf("\n");
    

    printf("Cadena1: %s\n", cadena1);
    printf("Length Cadena1: %ld\n", strlen(cadena1));
    printf("\n");

    //Ingresar string desde teclado
    printf("Ingrese un refrán: ");
    scanf("%[^\n]", conjunto);
    getchar();
    printf("Conjunto: %s\n", conjunto);
    printf("Length Conjunto: %ld\n", strlen(conjunto));
    printf("\n");

    printf("Ingrese el mismo refrán: ");
    fgets(cadena2, 50*sizeof(char), stdin);
    cadena2[strlen(cadena2)-1] = '\0';
    printf("Cadena2: %s\n", cadena2);
    printf("Length Cadena2: %ld\n", strlen(cadena2));
    printf("\n");

    char searchChar;
    printf("Ingrese el caracter que desee buscar desde el final:");
    scanf("%c", &searchChar);
    getchar();
    char *cadenaResulante = strchr(cadena2, searchChar);
    puts(cadenaResulante); 

    //printf("Numeros: %ls\n", numeros);
    //presentarArregloCaracteres(strlen(conjunto), conjunto);
    return 0;
}


void presentarArregloCaracteres(int limite, char string[limite]){
    for (int i = 0; i < limite; i++){
        printf("%c", string[i]);
    }
    printf("\n");
}

void ingresarArregloCaracteres(int limite, char string[limite]){
    for (int i = 0; i < limite; i++){
        scanf("%c", &string[i]);
        getchar();  
    }
}