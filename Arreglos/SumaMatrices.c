/**
 * @file SumaMatrices.c
 * @author wduck
 * @brief 
 * @version 0.1
 * @date 2024-02-20
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#include <stdio.h>
#include <stdlib.h>

struct OrdenMatriz{
    int fila, columnas;
};


int leerDimension();
void leerOrdenMatriz(int *fila, int *col);
void ingresarMatriz(char *mensaje, int filas, int columnas, float matriz[filas][columnas]);
void presentarMatriz(char *mensaje, int filas, int columnas, float matriz[filas][columnas]);
void sumarMatrices(int filas, int columnas, 
                            float matriz1[filas][columnas], 
                            float matriz2[filas][columnas], 
                            float matrizRta[filas][columnas]);
//struct OrdenMatriz leerOrdenMatriz();

int main(int argc, char const *argv[]){
    system("@cls||clear");
    //int filas = leerDimension("Ingrese las filas: ");
    //int columnas = leerDimension("Ingrese las columnas: ");
    //printf("Orden[%d, %d]\n", filas, columnas);
    int filas, columnas;
    leerOrdenMatriz(&filas, &columnas);
    printf("Orden [%d, %d]\n", filas, columnas);
    float matrizA[filas][columnas];
    float matrizB[filas][columnas];
    float matrizRta[filas][columnas];
    ingresarMatriz("Ingrese la matriz A: ", filas, columnas, matrizA);
    ingresarMatriz("Ingrese las matriz B: ", filas, columnas, matrizB);
    sumarMatrices(filas, columnas, matrizA, matrizB, matrizRta);

    presentarMatriz("A", filas, columnas, matrizA);
    printf( " + \n");
    presentarMatriz("B", filas, columnas, matrizB);
    printf( " = \n");
    presentarMatriz("RTA", filas, columnas, matrizRta);
    return 0;
}

void sumarMatrices(int filas, int columnas, 
                            float matriz1[filas][columnas], 
                            float matriz2[filas][columnas], 
                            float matrizRta[filas][columnas]){
    for (int i = 0; i < filas; i++){
        for (int j = 0; j < columnas; j++){
            matrizRta[i][j] = matriz1[i][j] + matriz2[i][j];
        }
    }
}

void ingresarMatriz(char *mensaje, int filas, int columnas, float matriz[filas][columnas]){
    printf("%s\n",mensaje);
    for (int i = 0; i < filas; i++){
        for (int j = 0; j < columnas; j++){
            printf("Ingrese el valor para la posicion [%d,%d]: " ,i,j);
            scanf("%f", &matriz[i][j]); 
        }
    }
}

void presentarMatriz(char *mensaje, int filas, int columnas, float matriz[filas][columnas]){
    printf("%s: \n", mensaje);
    for (int i = 0; i < filas; i++){
        for (int j = 0; j < columnas; j++){
            printf("%f\t", matriz[i][j]);
        }
        printf("\n");
    }
}

void leerOrdenMatriz(int *fila, int *columna){
    printf("Ingrese el orden de matriz (fil, col): ");
    scanf("%d,%d", fila, columna);
    getchar();
}



int leerDimension(char *mensaje){
    int dimension;
    printf("%s: ", mensaje);
    scanf("%d",&dimension);
    getchar();
    return  dimension;;
}

