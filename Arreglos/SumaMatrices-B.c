/**
 * @file SumaMatrices-b.c
 * @author wduck
 * @brief 
 * @version 0.1
 * @date 2024-02-20
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#include <stdio.h>
#include <stdlib.h>

struct OrdenMatriz
{
    int filas;
    int columnas;
};



int leerDimension(char *mensaje);
//struct OrdenMatriz leerDimensiones();
void leerDimensiones(int *filas, int *columnas);
void ingresarMatriz(char * mensaje, int filas, int columnas, int matriz[filas][columnas]);
void presentarMatriz(char * mensaje, int filas, int columnas, int matriz[filas][columnas]);
void sumarMatrices(int filas, int columnas, 
                    int matriz1[filas][columnas],
                    int matriz2[filas][columnas],
                    int matrizSuma[filas][columnas]);



int main(int argc, char const *argv[]){
    system("@cls||clear");
    int filas, columnas;
    //filas = leerDimension("Ingrese las filas");
    //columnas = leerDimension("Ingrese las columnas");
    //printf("Orden de Matriz [%d, %d]\n", filas, columnas);
    leerDimensiones(&filas, &columnas);
    int matrizA[filas][columnas];
    int matrizB[filas][columnas];
    int matrizRta[filas][columnas];

    printf("Orden de Matriz [%d, %d]\n", filas, columnas);
    ingresarMatriz("Ingrese la matriz A", filas, columnas, matrizA);
    ingresarMatriz("Ingrese la matriz B", filas, columnas, matrizB);

    sumarMatrices(filas, columnas, matrizA, matrizB, matrizRta);

    presentarMatriz("A", filas, columnas, matrizA);
    printf(" + \n");
    presentarMatriz("B", filas, columnas, matrizB);
    printf(" = \n");
    presentarMatriz("RTA", filas, columnas, matrizRta);
    return 0;
}

void sumarMatrices(int filas, int columnas, 
                    int matriz1[filas][columnas],
                    int matriz2[filas][columnas],
                    int matrizSuma[filas][columnas]){
    for (int i = 0; i < filas; i++){
        for (int j = 0; j < columnas; j++){
            matrizSuma[i][j] = matriz1[i][j] + matriz2[i][j];
        }
    }
}


void ingresarMatriz(char * mensaje, int filas, int columnas, int matriz[filas][columnas]){
    printf("%s:\n", mensaje);
    for (int i = 0; i < filas; i++){
        for (int j = 0; j < columnas; j++){
            printf("Ingrese el elemento [%d,%d]: ", i,j);
            scanf("%d", &matriz[i][j]);
        }
        printf("\n");
    }
}

void presentarMatriz(char * mensaje, int filas, int columnas, int matriz[filas][columnas]){
    printf("%s:\n", mensaje);
    for (int i = 0; i < filas; i++){
        for (int j = 0; j < columnas; j++){
            printf("%d\t", matriz[i][j]);
        }
        printf("\n");
    }
}


void leerDimensiones(int *filas, int *columnas){
    printf("Ingrese las dimensiones de las matrices de la forma fila, col: ");
    scanf("%d, %d",filas,columnas);
    getchar();
}


int leerDimension(char *mensaje){
    int dimension;
    printf("%s: ",mensaje);
    scanf("%d",&dimension);
    getchar();
    return dimension;
}
