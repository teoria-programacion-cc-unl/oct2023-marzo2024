/**
 * @brief 
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

struct Persona{
    char *nombres;
    char *apellidos;
};


char * ingresarTexto(char *mensaje);
struct Persona ingresarDatosPersonales();
char * obtenerNombresCompletos(struct Persona persona);
int contarVocales(char *texto);
int contarConsonantes(char *texto);
int esVocal(char character);

int main(int argc, char const *argv[]){
    system("@cls||clear");
    struct Persona persona = ingresarDatosPersonales();
    char *nombresCompletos = obtenerNombresCompletos(persona);
    
    printf("DATOS PERSONALES\n");
    printf("NOMBRES COMPLETOS: %s\n", nombresCompletos);
    printf("Persona.apellido: %s\n", persona.apellidos );
    printf("Persona.nombres: %s\n", persona.nombres );
    printf("Tus nombres tiene %d vocales ", contarVocales(nombresCompletos));
    printf("y %d consonantes\n", contarConsonantes(nombresCompletos));
    return 0;
}

int contarVocales(char *texto){
    int contador = 0;
    for (int i = 0; i < strlen(texto); i++){  
        char c = toupper(texto[i]);
        if (esVocal(texto[i])){
            contador++;
        }     
    }
    return contador;
}

int contarConsonantes(char *texto){
    int contador = 0;
    for (int i = 0; i < strlen(texto); i++){
        char leido = texto[i]; 
        if (isalpha(leido)){
            if (!esVocal(leido)){
                contador++;
            }
        } 
    }
    return contador;
}

int esVocal(char character){
    char *found = strrchr("AEIOUaeiou", character);
    return found != NULL;

    /*character = toupper(character);
    switch (character){
        case 'A': return 1;
        case 'E': return 1;
        case 'I': return 1;
        case 'O': return 1;
        case 'U': return 1;
        default:
            return 0;
    }*/
}

char * obtenerNombresCompletos(struct Persona persona){
    char *nombresCompletos = malloc(sizeof(persona.nombres) +  sizeof(persona.apellidos) + 1);
    //sprintf(nombresCompletos, "%s %s", persona.apellidos, persona.nombres);
    strcpy(nombresCompletos, persona.apellidos);
    strcat(nombresCompletos, " ");
    strcat(nombresCompletos, persona.nombres);
    for (int i = 0; i < strlen(nombresCompletos); i++){
        nombresCompletos[i] = toupper(nombresCompletos[i]);
    }
    return nombresCompletos;
}

struct Persona ingresarDatosPersonales(){
    struct Persona persona;
    persona.nombres = ingresarTexto("Ingrese sus nombres: ");
    persona.apellidos = ingresarTexto("Ingrese sus apellidos: ");
    return persona;
}

char * ingresarTexto(char *mensaje){
    char *texto = malloc(50 * sizeof(char));
    printf("%s", mensaje);
    fgets(texto, 50 * sizeof(char), stdin);
    texto[strlen(texto)-1] = '\0';
    return texto;
}


