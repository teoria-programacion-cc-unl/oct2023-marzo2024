/**
 * @file evaluacion.c
 * @author Cesrin y Yober
 * @brief Un tienda de víveres desea un programa que le ayude a generar facturas e imprimir las mismas
    hacia su cliente luego que el mismo haya terminado su compra.
 * @version 0.1
 * @date 2024-02-26
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#define cantidadCompras 100

struct Cliente{
    int id;
    char nombresApellidos [40];
    int dni;
    char correo[30];
    char direccion [30];
};

struct Item{
    char codigo[10];
    char descripcionProductos[cantidadCompras][20];
    int cantidad[cantidadCompras];
    int nroCompras;
    float precioUnitario[cantidadCompras];
    float precioTotal[cantidadCompras];
    
};

struct Factura{
    int numero;
    char fecha [11];
    float subTotal;
    float total;
    float iva;
};

void obtenerNombresApellidos(struct Cliente *cliente);
int pedirNumeroCedula(struct Cliente cliente);
void obtenerCorreo(struct Cliente *cliente);
void obterDireccion(struct Cliente *cliente);
int idCliente(struct Cliente cliente);

void pedirNroProductosComprados(struct Item *item);
void pedirCantidadDescripcionCompras(struct Item *item);
void generarCodigoProductos(struct Item *item);
void informacionProducto(struct Item *item);

void fechaFactura(struct Factura *factura);
void numeroFactura(struct Factura *Factura);
float subTotal(struct Factura factura, struct Item item);
float totalPagar(struct Factura *factura);

void presentarFactura(struct Cliente cliente, struct Item item, struct Factura factura);
void guardarFacturaEnArchivo(struct Cliente cliente, struct Item item, struct Factura factura);

int main(int argc, char const *argv[]){
    struct Cliente cliente;
    struct Item item;
    struct Factura factura;
    system("@cls||clear");
    obtenerNombresApellidos(&cliente);
    cliente.dni = pedirNumeroCedula(cliente);
    
    obtenerCorreo(&cliente);
    obterDireccion(&cliente);
    cliente.id = idCliente(cliente);

    pedirNroProductosComprados(&item);
    pedirCantidadDescripcionCompras(&item);
    informacionProducto(&item);
    generarCodigoProductos(&item);

    fechaFactura(&factura);
    numeroFactura(&factura);
    factura.subTotal = subTotal(factura, item);
    factura.total = totalPagar(&factura);
    system("@cls||clear");
    presentarFactura(cliente, item, factura);
    guardarFacturaEnArchivo(cliente, item, factura); 
    return 0;
}

void obtenerNombresApellidos(struct Cliente *cliente){
    printf("Ingrese sus nombres y apellidos:\n ");
    fgets(cliente->nombresApellidos, 40, stdin);
}

int pedirNumeroCedula(struct Cliente cliente){
    int numCedula;    
    printf("Ingrese su numero de Cedula\n");
    scanf("%i", &numCedula);
    getchar();
    return numCedula;
}

void obtenerCorreo(struct Cliente *cliente){
    printf("Ingrese su correo electronico:\n ");
    fgets(cliente->correo, 30, stdin);
}

void obterDireccion(struct Cliente *cliente){
    printf("Ingrese su direccion:\n ");
    fgets(cliente->direccion, 30, stdin);
}

int idCliente(struct Cliente cliente){
    int id;
    printf("Ingrese su ID usuario\n");
    scanf("%i", &id);
    getchar();
    return id;
}

void pedirNroProductosComprados(struct Item *item){
    printf("Ingrese el numero de productos comprados: ");
    scanf("%d", &(item->nroCompras));
    getchar();
}

void pedirCantidadDescripcionCompras(struct Item *item){
   printf("Ingrese el nombre y la cantidad de cada producto comprado\n");

    for (int i = 0; i < item->nroCompras; i++){
        printf("Nombre del producto %d: ", i + 1);
        fgets(item->descripcionProductos[i], 20, stdin); 

        printf("Cantidad de producto %s: ", item->descripcionProductos[i]);
        scanf("%i", &(item->cantidad[i]));
        getchar(); 
    }
}

void generarCodigoProductos(struct Item *item){
    srand(time(NULL));
    for (int i = 0; i < item->nroCompras; i++){
        item->codigo[i] = rand() % 1000;
    }
}

void informacionProducto(struct Item *item){
    for (int i = 0; i < item->nroCompras; i++){
        printf("Valor unitario para el producto %s: ", item->descripcionProductos[i]);
        scanf("%f", &(item->precioUnitario[i]));
        getchar();
        item->precioTotal[i] = item->precioUnitario[i] * item->cantidad[i];
    } 
}

void fechaFactura(struct Factura *factura){
    time_t t = time(NULL); 
    struct tm *tm = localtime(&t); 
    
    strftime(factura->fecha, sizeof(factura->fecha), "%Y/%m/%d", tm);
}

void numeroFactura(struct Factura *factura){
    srand(time(NULL));
    factura->numero = rand() % 1000000;
}

float subTotal(struct Factura factura, struct Item item){
    float sub_total = 0.0;
    for (int i = 0; i < item.nroCompras; i++){
        sub_total += item.precioTotal[i];
    }
    return sub_total;
}

float totalPagar(struct Factura *factura){
    float total_pagar = 0.0;
    factura->iva = factura->subTotal * 0.14;
    total_pagar = factura->iva + factura->subTotal;
    return total_pagar;    
}

void presentarFactura(struct Cliente cliente, struct Item item, struct Factura factura){
    printf("----------------------------------------------------------\n");
    printf("\t\tFACTURA DE VENTA\n");
    printf("\t\tEMPRESA TILINES STORE\n");
    printf("----------------------------------------------------------\n");
    printf("Fecha de emision: %s\n", factura.fecha);
    printf("Nro. factura: %05d\n\n", factura.numero);
    printf("Cliente\n");
    printf("----------------------------------------------------------\n");
    printf("Nombres: %s\n", cliente.nombresApellidos);
    printf("Cedula: %i\n", cliente.dni);
    printf("Correo: %s\n", cliente.correo);
    printf("Direccion: %s\n", cliente.direccion);
    printf("Id: %i\n", cliente.id);
    printf("----------------------------------------------------------\n");
    printf("LISTADO DE PRODUCTOS\n");
    printf("----------------------------------------------------------\n");
    printf("Codigo  Cantidad    Precio U.     Total      Descripcion\n\n");
    for (int i = 0; i < item.nroCompras; i++){
        printf("%i", item.codigo[i]);
        printf("\t%i", item.cantidad[i]);
        printf("\t\t%.2f", item.precioUnitario[i]);
        printf("\t%6.2f", item.precioTotal[i]);
        printf("\t\t%s", item.descripcionProductos[i]);
    }
    printf("----------------------------------------------------------\n");
    printf("Subtotal Neto : %.2f USD\n", factura.subTotal);
    printf("----------------------------------------------------------\n");
    printf("IVA(14%%) : %.2f\n", factura.iva);
    printf("----------------------------------------------------------\n");
    printf("Total a pagar : %.2f USD\n", factura.total);
    printf("----------------------------------------------------------\n");
    printf("!!!!!!!!GRACIAS POR CONFIAR EN TILINES STORE!!!!!!!\n");
    printf("----------------------------------------------------------\n");
    printf("==========================================================\n");
    printf("\tProgramado por Yober y Cesarin\n");
    printf("==========================================================\n");
    printf("\n");
}

void guardarFacturaEnArchivo(struct Cliente cliente, struct Item item, struct Factura factura){
    FILE *archivo;
    char nombreArchivo[50];
    sprintf(nombreArchivo, "factura_%d.txt", factura.numero); 
    archivo = fopen(nombreArchivo, "w");

    if (archivo == NULL){
        printf("Error al abrir el archivo.\n");
        return;
    }

    fprintf(archivo, "----------------------------------------------------------\n");
    fprintf(archivo, "\t\tFACTURA DE VENTA\n");
    fprintf(archivo, "\t\tEMPRESA TILINES STORE\n");
    fprintf(archivo, "----------------------------------------------------------\n");
    fprintf(archivo, "Fecha de emision: %s\n", factura.fecha);
    fprintf(archivo, "Nro. factura: %05d\n\n", factura.numero);
    fprintf(archivo, "Cliente\n");
    fprintf(archivo, "----------------------------------------------------------\n");
    fprintf(archivo, "Nombres: %s\n", cliente.nombresApellidos);
    fprintf(archivo, "Cedula: %i\n", cliente.dni);
    fprintf(archivo, "Correo: %s\n", cliente.correo);
    fprintf(archivo, "Dirección: %s\n", cliente.direccion);
    fprintf(archivo, "Id: %i\n", cliente.id);
    fprintf(archivo, "----------------------------------------------------------\n");
    fprintf(archivo, "LISTADO DE PRODUCTOS\n");
    fprintf(archivo, "----------------------------------------------------------\n");
    fprintf(archivo, "Codigo  Cantidad    Precio U.     Total      Descripcion\n\n");
    for (int i = 0; i < item.nroCompras; i++) {
        fprintf(archivo, "%i\t%i\t    %.2f\t%6.2f\t\t%s\n", item.codigo[i], item.cantidad[i], item.precioUnitario[i], item.precioTotal[i], item.descripcionProductos[i]);
    }
    fprintf(archivo, "----------------------------------------------------------\n");
    fprintf(archivo, "Subtotal Neto : %.2f USD\n", factura.subTotal);
    fprintf(archivo, "----------------------------------------------------------\n");
    fprintf(archivo, "IVA(14%%) : %.2f\n", factura.iva);
    fprintf(archivo, "----------------------------------------------------------\n");
    fprintf(archivo, "Total a pagar : %.2f USD\n", factura.total);
    fprintf(archivo, "----------------------------------------------------------\n");
    fprintf(archivo, "!!!!!!!!GRACIAS POR CONFIAR EN TILINES STORE!!!!!!!\n");
    fprintf(archivo, "----------------------------------------------------------\n");
    fprintf(archivo, "==========================================================\n");
    fprintf(archivo, "\tProgramado por Yober y Cesarin\n");
    fprintf(archivo, "==========================================================\n");
    fprintf(archivo, "\n");

    fclose(archivo);
}