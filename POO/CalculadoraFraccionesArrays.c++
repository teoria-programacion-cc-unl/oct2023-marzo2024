/**
 * @file CalculadoraFracciones.cpp
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2024-02-01
 * 
 * @copyright Copyright (c) 2024
 * 
 */


#include <stdlib.h>
#include <iostream>
using namespace std;


class Fraccion{
    private:
        int numerador;
        int denominador;

    public:
        //Constructor
        Fraccion(int numerador = 1, int denominador = 1){
            this->numerador = numerador;
            this->denominador = denominador;
        }
        // Destructor
        ~Fraccion(){
            //cout << "DESTRUCTOR" << endl;
        }

        float aPuntoFlotante(){
            return (float)this->numerador / (float)this->denominador;
        }

        void simplificate(){
            Fraccion *f = this->simplificar();
            this->numerador = f->numerador;
            this->denominador = f->denominador;
            f->~Fraccion();
        }

        void inviertete(){
            int aux = this->numerador;
            this->numerador = this->denominador;
            this->denominador = aux;
        }

        Fraccion *invertir(){
            Fraccion *clonada = new Fraccion(this->numerador, this->denominador);
            clonada->inviertete();
            return clonada;   
        }

        //TODO: implements
        Fraccion *sumar(Fraccion operando){
            return new Fraccion(3,4);
        }

        //TODO: implements
        Fraccion *restar(Fraccion operando){
            return new Fraccion(3,4);
        }

        Fraccion *multiplicar(Fraccion operando){
            int _num = this->numerador * operando.getNumerador();
            int _den = this->denominador * operando.getDenominador();
            Fraccion *rta = new Fraccion(_num,_den);
            rta->simplificate();
            return rta;
        }

        Fraccion *dividir(Fraccion operando){
            Fraccion *f = operando.invertir();
            return multiplicar(*f);
        } 

        Fraccion *simplificar(){
            Fraccion *fraccionCopia = new Fraccion(this->numerador, this->denominador);
            if (fraccionCopia->denominador == 0){
                fraccionCopia->numerador = 0;
                return fraccionCopia;
            }

            int menor = abs(fraccionCopia->numerador);
            if (menor > abs(fraccionCopia->denominador)){
                menor = abs(fraccionCopia->denominador);
            }

            for (int i = menor; i > 1; i--){
                if (fraccionCopia->numerador % i == 0 &&  fraccionCopia->denominador % i == 0){
                    fraccionCopia->numerador  = fraccionCopia->numerador / i;
                    fraccionCopia->denominador = fraccionCopia->denominador / i;
                    break;
                }
            }
            return fraccionCopia;
        }


        //GETTER/SETTER
        int getNumerador(){
            return numerador;
        }

        void setNumerador(int numerador){
            this->numerador = numerador;
        }

        int getDenominador(){
            return this->denominador;
        }

        void setDenominador(int valor){
            this->denominador = valor;
        }

       string toString(){
            return "[" + to_string(this->numerador) + " / " + to_string(this->denominador) + "]";
           }
} ;

class CalculadoraFracciones {
    private:
        /* data */
        Fraccion *operandos;
        int numeroOperandos;
        char operador; 

        Fraccion *sumar(){
            Fraccion aux = operandos[0];    
            Fraccion *result = new Fraccion(aux.getNumerador(), aux.getDenominador());
            for (int i = 1; i < this->numeroOperandos; i++){
                result = result->sumar(operandos[i]);
            }
            return result;
        }

        Fraccion *restar(){
            Fraccion aux = operandos[0];    
            Fraccion *result = new Fraccion(aux.getNumerador(), aux.getDenominador());
            for (int i = 1; i < this->numeroOperandos; i++){
                result = result->restar(operandos[i]);
            }
            return result;
        }
        
        Fraccion *multiplicar(){
            Fraccion aux = operandos[0];    
            Fraccion *result = new Fraccion(aux.getNumerador(), aux.getDenominador());
            for (int i = 1; i < this->numeroOperandos; i++){
                result = result->multiplicar(operandos[i]);
            }
            return result;
        }

        Fraccion *dividir(){
            Fraccion aux = operandos[0];    
            Fraccion *result = new Fraccion(aux.getNumerador(), aux.getDenominador());
            for (int i = 1; i < this->numeroOperandos; i++){
                result = result->dividir(operandos[i]);
            }
            return result;
        }    
    
    public:
        
        CalculadoraFracciones(){
            this->numeroOperandos = 2;
            this->operandos = new Fraccion[this->numeroOperandos];    
            operador = '=';
        }
        CalculadoraFracciones(Fraccion *arreglo, int numeroOperandos){
            this->operandos = arreglo;
            this->numeroOperandos = numeroOperandos;
            operador = '=';
        }
             
        ~CalculadoraFracciones(){

        };

        Fraccion *calcular(char operador){
            this->operador = operador;
            Fraccion *rta;
            switch(this->operador){
                case '+': { 
                    rta = this->sumar();
                    break;
                }
                case '-': { 
                    rta = this->restar();
                    break;
                }
                case '*': { 
                    rta = this->multiplicar();
                    break;
                }
                case '/': { 
                    rta = this->dividir();
                    break;
                }                
                default: {
                    rta = NULL;
                }
                
            }
            return rta;
        }

        Fraccion *getOperandos(){
            return this->operandos;
        }

        void setOperandos(Fraccion *fracciones){
            this->operandos = fracciones;
        }

        string toString(){
            string str = "";
            for(int i=0; i < this->numeroOperandos; i++ ){
                str = str + this->operandos[i].toString() + " " + operador + " ";
                //str = str.append(this->operandos[i].toString() + " " + operador + " ");
            }
            return str;
        }
};


Fraccion *ingresarFraccion(string mensaje);
CalculadoraFracciones *construirCalculadoraFracciones();
char elegirOperacion();

int main(int argc, char const *argv[]){
    system("@cls||clear");
    CalculadoraFracciones *cf = construirCalculadoraFracciones();
    char operacion = elegirOperacion();
    Fraccion *rta = cf->calcular(operacion);
    cout << "Resultado:: " << endl;
    if (rta != NULL) {
        cout << cf->toString() << " = " << rta->toString() << endl;
    } else {
        cout <<"No existe operacion definida" << endl;
    }

    return 0;
}

char elegirOperacion(){
    char op;
    cout << "Ingrese la operacion que desee operar [ + , - , *, /]: " << endl;
    cin >> op;
    return op;
}

CalculadoraFracciones *construirCalculadoraFracciones(){
    int numeroOperandos;
    cout << "Cuantos fracciones necesita: ";
    cin >> numeroOperandos;
    int tamanioMemoria = numeroOperandos * sizeof(Fraccion);
    cout << tamanioMemoria << endl;
    //Fraccion fracciones[numeroOperandos]; 
    Fraccion *fracciones = (Fraccion *)malloc(tamanioMemoria);
    for (int i = 0; i < numeroOperandos; i++){
        char *mensaje = (char *)malloc(30 * sizeof(char));
        sprintf(mensaje, "Ingrese fraccion No. %d", i+1);
        Fraccion *f = ingresarFraccion(mensaje);
        fracciones[i] = *f; 
    }
    CalculadoraFracciones *cf = new CalculadoraFracciones(fracciones, numeroOperandos);
    return cf;
}


Fraccion *ingresarFraccion(string mensaje){
    int num, deno;
    cout << mensaje << ": " <<  endl;
    cout << "Numerador: " << endl;
    cin >> num;
    cout << "Denominador: " << endl;
    cin >> deno;
    return new Fraccion(num,deno);
}
