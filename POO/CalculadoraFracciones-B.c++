/**
 * @file CalculadoraFracciones-B.c++
 * @author wduck
 * @brief Demostrar con un programa de la calculadora de fracccions la POO
 * @version 0.1
 * @date 2024-02-02
    *   * @copyright Copyright (c) 2024
    * 
    */

#include <iostream>
using namespace std;

class Fraccion {
    private:
        int numerador;
        int denominador;
    
    public:
        // Constructor
        Fraccion(int numerador = 1, int denominador = 1){
            //cout << "CONSTRUCTOR CON ARGUMETOS" << endl;
            this->numerador = numerador;
            this->denominador = denominador;
        }
        /*
        Fraccion(){
            cout << "CONSTRUCTOR SIN ARGUMETOS" << endl;
            this->numerador = 1;
            this->denominador = 1;
        }

        Fraccion(int numerador, int denominador){
            cout << "CONSTRUCTOR CON ARGUMETOS" << endl;
            this->numerador = numerador;
            this->denominador = denominador;
        }
        */

        // Destructor
        ~Fraccion(){
            //cout << "DESTRUCTOR" << endl;
        }

        Fraccion *multiplicar(Fraccion *operando){
            int numerador = operando->getNumerador() * this->numerador;
            int denominador = operando->getDenominador() * this->denominador;
            Fraccion *f =  new Fraccion(numerador, denominador);
            f->simplificate();
            return f;
        }

        Fraccion *dividir(Fraccion *operando){
            Fraccion *fInv = operando->invertir();
            Fraccion *rta = multiplicar(fInv);
            return rta;
        }

        //TODO: implementar
        Fraccion *sumar(Fraccion *operando){
            
            return new Fraccion(8,12);
        }

        //TODO: implementar
        Fraccion *restar(Fraccion *operando){
            
            return new Fraccion(8,12);
        }

        void inviertete(){
            int aux = this->numerador;
            this->numerador = this->denominador;
            this->denominador = aux;
        }

        Fraccion *invertir(){
            Fraccion *clonada = new Fraccion(this->numerador, this->denominador);
            clonada->inviertete();
            return clonada;   
        }

        void simplificate(){
            Fraccion *f = this->simplificar();
            this->numerador = f->getNumerador();
            this->denominador = f->getDenominador();
            f->~Fraccion();
        }

        Fraccion *simplificar(){
            Fraccion *fraccionCopia = new Fraccion(this->numerador, this->denominador);
            if (fraccionCopia->denominador == 0){
                fraccionCopia->numerador = 0;
                return fraccionCopia;
            }
            int menor = abs(fraccionCopia->numerador);
            if (menor > abs(fraccionCopia->denominador)){
                menor = abs(fraccionCopia->denominador);
            }

            int i = menor;
            while (i >  1){
                if (fraccionCopia->numerador % i == 0 && fraccionCopia->denominador % i ==0){
                    fraccionCopia->numerador =fraccionCopia->numerador / i;
                    fraccionCopia->denominador = fraccionCopia->denominador / i;
                } else {
                    i--;
                }
            }
            return fraccionCopia;
        }

        float aPuntoFlotante(){
            return (float)this->numerador / (float)this->denominador;
        }

        // GETTER/SETTER
        int getNumerador(){
            return this->numerador;
        }

        void setNumerador(int numerador) { 
            this->numerador = numerador;
        } 

        int getDenominador(){
            return this->denominador;
        }

        void setDenominador(int denomnador){
            this->denominador = denominador;
        }

        string toString(){
            return "[ " + to_string(this->numerador) + " / " + to_string(this->denominador) + " ]";
        }

};

class CalculadoraFracciones{
    private:
        Fraccion *operando1, *operando2;
        char operacion;

        Fraccion *multiplicar(){
            return this->operando1->multiplicar(this->operando2);
        }

        Fraccion *dividir(){
            return this->operando1->dividir(this->operando2);
        }

        Fraccion *sumar(){
            return this->operando1->sumar(this->operando2);
        }

        Fraccion *restar(){
            return this->operando1->restar(this->operando2);
        }
    
    public:
        CalculadoraFracciones(Fraccion *operando1 = new Fraccion(), Fraccion *operando2 = new Fraccion()){
            this->operando1 = operando1;
            this->operando2 =  operando2;
            operacion = ',';
        }

        /*CalculadoraFracciones(){
            this->operando1 = new Fraccion();
            this->operando2 = new Fraccion();
        }

        CalculadoraFracciones(Fraccion *operando1, Fraccion *operando2 ){
            this->operando1 = operando1;
            this->operando2 =  operando2;
        }*/

        ~CalculadoraFracciones();

        Fraccion *calcular(char operacion){
            this->operacion = operacion;
            Fraccion *rta;
            switch (this->operacion){
                case '+': {
                    rta = this->sumar();
                    break;
                }
                case '-': {
                    rta = this->restar();
                    break;
                }
                case '*': {
                    rta = this->multiplicar();
                    break;
                }
                case '/': {
                    rta = this->dividir();
                    break;
                }
            
                default:
                    rta = NULL;
                    break;
            }
            return rta;
        }
        
        
        Fraccion *getOperando1(){
            return this->operando1;
        }

        void setOperando1(Fraccion *fraccion){
            this->operando1 = fraccion;
        }

        Fraccion *getOperando2(){
            return this->operando2;
        }

        void setOperando2(Fraccion *fraccion){
            this->operando2 = fraccion;
        }

        char getOperacion(){
            return this->operacion;
        }

        string toString(){
            return this->operando1->toString() + " " + this->operacion + " " + this->operando2->toString(); 
        }
};



Fraccion *ingresarFraccion(string mensaje);
CalculadoraFracciones *construirCalculadoraFraccion();
char elegirOperacion();

int main(int argc, char const *argv[]){
    system("@cls||clear");    
    CalculadoraFracciones *cf = construirCalculadoraFraccion();
    char operacion = elegirOperacion();

    Fraccion *rta = cf->calcular(operacion);
    cout << cf->toString() << " = ";
    if (rta != NULL) {
        cout << rta->toString() << " ===> " << rta->aPuntoFlotante() << endl;        
    } else {
        cout << "No existe resultado para esa operación: " << cf->getOperacion() << endl;
    }    

    return 0;
}

char elegirOperacion(){
    char operacion;
    cout << "Elija la operacion que desee realizar [ + , - , * , /]: "; 
    cin >> operacion;
    return operacion;
}

CalculadoraFracciones *construirCalculadoraFraccion(){
    Fraccion *f1 = ingresarFraccion("Ingrese la fraccion Nro. 1");
    Fraccion *f2 = ingresarFraccion("Ingrese la fraccion Nro. 3");
    CalculadoraFracciones *cf = new CalculadoraFracciones(f1,f2);
    return cf; 
}

Fraccion *ingresarFraccion(string mensaje){
    int num, deno;
    cout << mensaje << ": " << endl;
    cout << "Numerador: ";
    cin >> num;
    cout << "Denominador: ";
    cin >> deno;
    return new Fraccion(num, deno);
}
