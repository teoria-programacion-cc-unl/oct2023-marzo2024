/**
 * @file CalculadoraFracciones.cpp
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2024-02-01
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#include <iostream>
using namespace std;


class Fraccion{
    private:
        int numerador;
        int denominador;

    public:
        //Constructor
        Fraccion(int numerador = 1, int denominador = 1){
            this->numerador = numerador;
            this->denominador = denominador;
        }
        // Destructor
        ~Fraccion(){
            //cout << "DESTRUCTOR" << endl;
        }

        float aPuntoFlotante(){
            return (float)this->numerador / (float)this->denominador;
        }

        void simplificate(){
            Fraccion *f = this->simplificar();
            this->numerador = f->numerador;
            this->denominador = f->denominador;
            f->~Fraccion();
        }

        void inviertete(){
            int aux = this->numerador;
            this->numerador = this->denominador;
            this->denominador = aux;
        }

        Fraccion *invertir(){
            Fraccion *clonada = new Fraccion(this->numerador, this->denominador);
            clonada->inviertete();
            return clonada;   
        }

        //TODO: implements
        Fraccion *sumar(Fraccion operando){
            return new Fraccion(3,4);
        }

        //TODO: implements
        Fraccion *restar(Fraccion operando){
            return new Fraccion(3,4);
        }

        Fraccion *multiplicar(Fraccion operando){
            int _num = this->numerador * operando.getNumerador();
            int _den = this->denominador * operando.getDenominador();
            Fraccion *rta = new Fraccion(_num,_den);
            rta->simplificate();
            return rta;
        }

        Fraccion *dividir(Fraccion operando){
            Fraccion *f = operando.invertir();
            return multiplicar(*f);
        } 

        Fraccion *simplificar(){
            Fraccion *fraccionCopia = new Fraccion(this->numerador, this->denominador);
            if (fraccionCopia->denominador == 0){
                fraccionCopia->numerador = 0;
                return fraccionCopia;
            }

            int menor = abs(fraccionCopia->numerador);
            if (menor > abs(fraccionCopia->denominador)){
                menor = abs(fraccionCopia->denominador);
            }

            for (int i = menor; i > 1; i--){
                if (fraccionCopia->numerador % i == 0 &&  fraccionCopia->denominador % i == 0){
                    fraccionCopia->numerador  = fraccionCopia->numerador / i;
                    fraccionCopia->denominador = fraccionCopia->denominador / i;
                    break;
                }
            }
            return fraccionCopia;
        }


        //GETTER/SETTER
        int getNumerador(){
            return numerador;
        }

        void setNumerador(int numerador){
            this->numerador = numerador;
        }

        int getDenominador(){
            return this->denominador;
        }

        void setDenominador(int valor){
            this->denominador = valor;
        }

       string toString(){
            return "[" + to_string(this->numerador) + " / " + to_string(this->denominador) + "]";
           }
} ;

class CalculadoraFracciones {
    private:
        /* data */
        Fraccion *operando1, *operando2;
        char operador; 

        Fraccion *sumar(){
            return operando1->sumar(*operando2);
        }

        Fraccion *restar(){
            return operando1->restar(*operando2);
        }
        
        Fraccion *multiplicar(){
            return operando1->multiplicar(*operando2);
        }

        Fraccion *dividir(){
            return operando1->dividir(*operando2);
        }    
    
    public:
        
        CalculadoraFracciones(){
            this->operando1 = new Fraccion();
            this->operando2 = new Fraccion();
            operador = '=';
        }
        CalculadoraFracciones(Fraccion *f1, Fraccion *f2){
            this->operando1 = f1;
            this->operando2 = f2;
            operador = '=';
        }
             
        ~CalculadoraFracciones(){

        };

        Fraccion *calcular(char operador){
            this->operador = operador;
            Fraccion *rta;
            switch(this->operador){
                case '+': { 
                    rta = this->sumar();
                    break;
                }
                case '-': { 
                    rta = this->restar();
                    break;
                }
                case '*': { 
                    rta = this->multiplicar();
                    break;
                }
                case '/': { 
                    rta = this->dividir();
                    break;
                }
                
                default: {
                    rta = NULL;
                }
                
            }
            return rta;
        }

        Fraccion *getOperando1(){
            return this->operando1;
        }

        void setOperando1(Fraccion *fraccion){
            this->operando1 = fraccion;
        }

        Fraccion *getOperando2(){
            return this->operando2;
        }

        void setOperando2(Fraccion *fraccion){
            this->operando2 = fraccion;
        }

        string toString(){
            return this->operando1->toString() + " " + operador + " " + this->operando2->toString();  
        }
};


Fraccion *ingresarFraccion(string mensaje);
CalculadoraFracciones *construirCalculadoraFracciones();
char elegirOperacion();

int main(int argc, char const *argv[]){
    system("@cls||clear");
    CalculadoraFracciones *cf = construirCalculadoraFracciones();
    char operacion = elegirOperacion();
    Fraccion *rta = cf->calcular(operacion);
    if (rta != NULL) {
        cout << cf->toString() << " = " << rta->toString() << endl;
    } else {
        cout <<"No existe operacion definida" << endl;
    }

    return 0;
}

char elegirOperacion(){
    char op;
    cout << "Ingrese la operacion que desee operar [ + , - , *, /]: " << endl;
    cin >> op;
    return op;
}

CalculadoraFracciones *construirCalculadoraFracciones(){
    Fraccion *f1 = ingresarFraccion("Ingrese fraccion No. 1");
    Fraccion *f2 = ingresarFraccion("Ingrese fraccion No. 2");
    CalculadoraFracciones *cf = new CalculadoraFracciones(f1, f2);
    return cf;
}


Fraccion *ingresarFraccion(string mensaje){
    int num, deno;
    cout << mensaje << ": " <<  endl;
    cout << "Numerador: " << endl;
    cin >> num;
    cout << "Denominador: " << endl;
    cin >> deno;
    return new Fraccion(num,deno);
}
