/**
 * @file Principal.c++
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2024-02-06
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#include <iostream>
using namespace std;

#include "lib/CalculadoraFracciones.h"

Fraccion *ingresarFraccion(string mensaje);
CalculadoraFracciones *construirCalculadoraFracciones();
char elegirOperacion();

int main(int argc, char const *argv[]){
    system("@cls||clear");
    CalculadoraFracciones *cf = construirCalculadoraFracciones();
    char operacion = elegirOperacion();
    Fraccion *rta = cf->calcular(operacion);
    if (rta != NULL) {
        cout << cf->toString() << " = " << rta->toString() << endl;
    } else {
        cout <<"No existe operacion definida" << endl;
    }

    return 0;
}

char elegirOperacion(){
    char op;
    cout << "Ingrese la operacion que desee operar [ + , - , *, /]: " << endl;
    cin >> op;
    return op;
}

CalculadoraFracciones *construirCalculadoraFracciones(){
    Fraccion *f1 = ingresarFraccion("Ingrese fraccion No. 1");
    Fraccion *f2 = ingresarFraccion("Ingrese fraccion No. 2");
    CalculadoraFracciones *cf = new CalculadoraFracciones(f1, f2);
    return cf;
}


Fraccion *ingresarFraccion(string mensaje){
    int num, deno;
    cout << mensaje << ": " <<  endl;
    cout << "Numerador: " << endl;
    cin >> num;
    cout << "Denominador: " << endl;
    cin >> deno;
    return new Fraccion(num,deno);
}