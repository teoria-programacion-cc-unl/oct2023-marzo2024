/**
 * @file CalculadoraFracciones.h
 * @author wduck
 * @brief 
 * @version 0.1
 * @date 2024-02-06
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#ifndef _CALCULADORA_FRACCIONES_H
#define _CALCULADORA_FRACCIONES_H

#include <iostream>
using namespace std;

#include "Fraccion.h"

class CalculadoraFracciones {
    private:
        /* data */
        Fraccion *operando1, *operando2;
        char operador; 

        Fraccion *sumar(){
            return operando1->sumar(*operando2);
        }

        Fraccion *restar(){
            return operando1->restar(*operando2);
        }
        
        Fraccion *multiplicar(){
            return operando1->multiplicar(*operando2);
        }

        Fraccion *dividir(){
            return operando1->dividir(*operando2);
        }    
    
    public:
        
        CalculadoraFracciones(){
            this->operando1 = new Fraccion();
            this->operando2 = new Fraccion();
            operador = '=';
        }
        CalculadoraFracciones(Fraccion *f1, Fraccion *f2){
            this->operando1 = f1;
            this->operando2 = f2;
            operador = '=';
        }
             
        ~CalculadoraFracciones(){

        };

        Fraccion *calcular(char operador){
            this->operador = operador;
            Fraccion *rta;
            switch(this->operador){
                case '+': { 
                    rta = this->sumar();
                    break;
                }
                case '-': { 
                    rta = this->restar();
                    break;
                }
                case '*': { 
                    rta = this->multiplicar();
                    break;
                }
                case '/': { 
                    rta = this->dividir();
                    break;
                }
                
                default: {
                    rta = NULL;
                }
                
            }
            return rta;
        }

        Fraccion *getOperando1(){
            return this->operando1;
        }

        void setOperando1(Fraccion *fraccion){
            this->operando1 = fraccion;
        }

        Fraccion *getOperando2(){
            return this->operando2;
        }

        void setOperando2(Fraccion *fraccion){
            this->operando2 = fraccion;
        }

        string toString(){
            return this->operando1->toString() + " " + operador + " " + this->operando2->toString();  
        }
};

#endif