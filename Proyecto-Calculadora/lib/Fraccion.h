/**
 * @file Fraccion.h
 * @author wduck
 * @brief 
 * @version 0.1
 * @date 2024-02-06
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#ifndef _FRACCION_H
#define _FRACCION_H

#include <iostream>
using namespace std;

class Fraccion{
    private:
        int numerador;
        int denominador;

    public:
        //Constructor
        Fraccion(int numerador = 1, int denominador = 1){
            this->numerador = numerador;
            this->denominador = denominador;
        }
        // Destructor
        ~Fraccion(){
            //cout << "DESTRUCTOR" << endl;
        }

        float aPuntoFlotante(){
            return (float)this->numerador / (float)this->denominador;
        }

        void simplificate(){
            Fraccion *f = this->simplificar();
            this->numerador = f->numerador;
            this->denominador = f->denominador;
            f->~Fraccion();
        }

        void inviertete(){
            int aux = this->numerador;
            this->numerador = this->denominador;
            this->denominador = aux;
        }

        Fraccion *invertir(){
            Fraccion *clonada = new Fraccion(this->numerador, this->denominador);
            clonada->inviertete();
            return clonada;   
        }

        //TODO: implements
        Fraccion *sumar(Fraccion operando){
            return new Fraccion(3,4);
        }

        //TODO: implements
        Fraccion *restar(Fraccion operando){
            return new Fraccion(3,4);
        }

        Fraccion *multiplicar(Fraccion operando){
            int _num = this->numerador * operando.getNumerador();
            int _den = this->denominador * operando.getDenominador();
            Fraccion *rta = new Fraccion(_num,_den);
            rta->simplificate();
            return rta;
        }

        Fraccion *dividir(Fraccion operando){
            Fraccion *f = operando.invertir();
            return multiplicar(*f);
        } 

        Fraccion *simplificar(){
            Fraccion *fraccionCopia = new Fraccion(this->numerador, this->denominador);
            if (fraccionCopia->denominador == 0){
                fraccionCopia->numerador = 0;
                return fraccionCopia;
            }

            int menor = abs(fraccionCopia->numerador);
            if (menor > abs(fraccionCopia->denominador)){
                menor = abs(fraccionCopia->denominador);
            }

            for (int i = menor; i > 1; i--){
                if (fraccionCopia->numerador % i == 0 &&  fraccionCopia->denominador % i == 0){
                    fraccionCopia->numerador  = fraccionCopia->numerador / i;
                    fraccionCopia->denominador = fraccionCopia->denominador / i;
                    break;
                }
            }
            return fraccionCopia;
        }

        //GETTER/SETTER
        int getNumerador(){
            return numerador;
        }

        void setNumerador(int numerador){
            this->numerador = numerador;
        }

        int getDenominador(){
            return this->denominador;
        }

        void setDenominador(int valor){
            this->denominador = valor;
        }

       string toString(){
            return "[" + to_string(this->numerador) + " / " + to_string(this->denominador) + "]";
           }
} ;
#endif