/*
	Author: WIlman CHamba Z. (wduck)
*/

#include <stdio.h>
#include <stdlib.h>

int main(){
	system("@cls||clear");

	int lado;
	int perimetro, area;
	
	printf("Ingrese el lado de un cuadrado: ");
	scanf("%i", &lado);
	getchar();
	
	perimetro = 4 * lado;
	area = lado * lado;
	
	printf("El perimetro del cuadrado es: %i\n", perimetro);
	printf("El área del cuadrado es: %i\n", area);
	
	printf("Presione cualquier tecla para cerrar..)");
	getchar();
	return 0;
}
