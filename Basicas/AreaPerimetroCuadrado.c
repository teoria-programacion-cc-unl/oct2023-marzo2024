/*
	Author: Wilman Chamba (wduck)
*/

#include <stdio.h>
#include <stdlib.h>

int main(){
	int lado;
	int perimetro, area;

	system("@cls||clear");
	printf("Ingrese el lado de un cuadrado: ");
	scanf("%i", &lado);
	getchar();
	
	perimetro = 4 * lado;
	area = lado * lado;
	
	printf("El perimetro del cuadrado es: %i\n", perimetro);
	printf("El area del cuadrado es: %i\n", area);

	printf("Presione cualquier tecla para cerrar..)");
	getchar();
	
	return 0;
}
