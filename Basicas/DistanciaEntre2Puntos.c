/**
 * @file DistanciaEntre2Puntros.c
 * @author wduck
 * @brief Calcula distancia, pendiente entre 2 puntos
 * @version 0.1
 * @date 2023-11-23
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

struct Punto{
    int x;
    int y;
};

int main(int argc, char const *argv[]){
    system("@cls||clear");
    
    struct Punto p1, p2;
    double pendiente, distancia;

    printf("Ingrese las coordenadas del punto inicial [Ejemplo x,y]: ");
    scanf("%d,%d",&p1.x,&p1.y);
    getchar();

    printf("Ingrese las coordenadas del punto final [Ejemplo x,y]: ");
    scanf("%d,%d",&p2.x,&p2.y);
    getchar();

    distancia = sqrt(pow((p2.x - p1.x),2) + pow((p2.y -p1.y), 2));
    pendiente = (float)(p2.y - p1.y) / (float)(p2.x - p1.x); 

    printf("Distancia[P1(%d,%d) -> P2(%d,%d)]: %lf\n", p1.x, p1.y, p2.x, p2.y, distancia);
    printf("Pendiente[P1(%d,%d) -> P2(%d,%d)]: %lf\n", p1.x, p1.y, p2.x, p2.y, pendiente);

    printf("Presione cualquier tecla para cerrar..)");
	getchar();

    return 0;
}
