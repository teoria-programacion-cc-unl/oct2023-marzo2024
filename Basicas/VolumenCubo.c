/**
 * @file VolumenCubo.c
 * @author Wilman Chamba
 * @brief Calcular el area, perimetro y volumen de un cubo
 * @version 0.1
 * @date 2023-11-22
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define NUMERO_CARAS 6


int main(int argc, char const *argv[]){
    system("@cls||clear");
    const float NUMERO_LADOS = 12;
    float lado, perimetro, area, volumen;
    printf("Ingrese la medida del lado: ");
    scanf("%f", &lado);
    getchar();
    perimetro = lado * NUMERO_LADOS;
    area = NUMERO_CARAS * powf(lado, 2);
    volumen = powf(lado, 3);

    printf ("Perimetro total: \t\t%.2f\n", perimetro);
    printf ("Area total: \t\t\t%.2f\n", area);
    printf ("Volumen total: \t\t\t%.2f\n", volumen);

    printf ("Gracias por usar nuestro programa!!");
    getchar();
    return 0;
}
