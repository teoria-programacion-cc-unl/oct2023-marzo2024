/**
 * @file AreaVolumenCubo.c
 * @author wduck
 * @brief Calcular el perimetro, area y volumen de un cubo 
 * @version 0.1
 * @date 2023-11-23
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define NUMERO_LADOS 12

int main(int argc, char const *argv[]){
    system("@cls||clear");
    
    const short NUMERO_CARAS = 6;
    
    float lado, perimetro, area, volumen;
    printf("CALCULADOR DE PERIMETRO, AREA Y VOLUME DE UN CUBO\n");
    printf("===================================================\n");

    printf("Ingrese el lado del cubo: ");
    scanf("%f", &lado);
    getchar();

    perimetro = lado * NUMERO_LADOS;
    area = powf(lado, 2) * NUMERO_CARAS;
    volumen = powf(lado,3);
    
    printf("Perimetro: \t\t%.2f\n", perimetro);
    printf("Área: \t\t\t%.2f\n", area);
    printf("Volumen: \t\t%.2f\n", volumen);

    printf("Gracias por usar nuestro programa, presiona cualquier tecla para salir ...");
    getchar();
    
    return 0;
}
