/**
 * @file DistanciaEntre2Puntos-B.c
 * @author wilman chamba @wduck
 * @brief Calcular la distancia, pendiente y angulo de inclinación entre 2 puntos
 * @version 0.1
 * @date 2023-11-24
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

struct Punto{
    int x;
    int y;
};

int main(int argc, char const *argv[]){
    system("@cls||clear");
    
    struct Punto p1, p2;
    double distancia, pendiente;

    printf("Ingresar las coordenas punto inicial (Por ejemplo: x, y): ");
    scanf("%d,%d", &p1.x, &p1.y);

    printf("Ingresar las coordenas punto final (Por ejemplo: x, y): ");
    scanf("%d,%d", &p2.x, &p2.y);

    distancia = sqrt(powf((p2.x - p1.x), 2) + powf((p2.y - p1.y), 2));
    pendiente = (float)(p2.y - p1.y) / (float)(p2.x - p1.x);

    printf("Distancia [P1(%d,%d) -> P2(%d,%d)]:\t%.3lf\n", p1.x, p1.y, p2.x, p2.y, distancia);
    printf("Pendiente [P1(%d,%d) -> P2(%d,%d)]:\t%.3lf\n", p1.x, p1.y, p2.x, p2.y, pendiente);

    printf("Presione cualquier tecla para cerrar..)");
	getchar();

    return 0;
}
