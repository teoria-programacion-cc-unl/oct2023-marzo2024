/**
 * @file EdacExacta-A.c
 * @author your name (you@domain.com)
 * @brief Calcular la edad exacta de una persona
 * @version 0.1
 * @date 2023-12-06
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int main(int argc, char const *argv[]){
    system("@cls||clear");

    struct tm fechaNacimiento;
    struct tm fechaActual;
    struct tm edadExacta;

    short prestamoMes, prestamoAnio;

    printf("Ingrese su fecha nacimiento (yyyy-mm-dd): ");
    scanf("%d-%d-%d", &fechaNacimiento.tm_year, &fechaNacimiento.tm_mon, &fechaNacimiento.tm_mday);
    getchar();

    // ObtenerFechaActual desde el sistema;
    time_t tiempoSegundos = time(NULL);
    fechaActual = *localtime(&tiempoSegundos);
    fechaActual.tm_year = fechaActual.tm_year + 1900;
    fechaActual.tm_mon = fechaActual.tm_mon + 1;
    // FIn ObtenerFechaActual desde el sistema;

    prestamoAnio = 0;
    prestamoMes = 0;

    // Calculo de los días
    edadExacta.tm_mday = fechaActual.tm_mday - fechaNacimiento.tm_mday;
    if (edadExacta.tm_mday < 0){
        edadExacta.tm_mday = edadExacta.tm_mday + 30;
        prestamoMes = prestamoMes + 1;
    }

    // Calculo del mes
    edadExacta.tm_mon = (fechaActual.tm_mon - prestamoMes) - fechaNacimiento.tm_mon;
    if (edadExacta.tm_mon < 0){
        edadExacta.tm_mon = edadExacta.tm_mon + 12;
        prestamoAnio = prestamoAnio + 1;
    }

    edadExacta.tm_year = (fechaActual.tm_year - prestamoAnio) - fechaNacimiento.tm_year;
    if (edadExacta.tm_year > 0){
        printf("Su edad es: %d anio(s), %d mes(es) y %d día(s)\n", edadExacta.tm_year, edadExacta.tm_mon, edadExacta.tm_mday); 
    } else {
        printf("Ud aún no ha nacido, esta en proyecto!!\n");
    }
    printf("Para la actual fecha: %d-%d-%d\n", fechaActual.tm_year, fechaActual.tm_mon, fechaActual.tm_mday);

    printf("Gracias por usar nuestro programa!!\n");
    getchar();
    return 0;
}
