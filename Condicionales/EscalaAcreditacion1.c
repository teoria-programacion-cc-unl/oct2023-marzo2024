/**
 * @file EscalaAcreditacion1.c
 * @author wduck
 * @brief 
 * @version 0.1
 * @date 2023-12-05
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include <stdio.h>
#include <stdlib.h>


int main(int argc, char const *argv[]){
    system("@cls||clear");
    
    float nota_TC_EC, nota_LPC, nota_AA, nota_EV;
    float notaFinal;
    char* estado; // Texto

    printf("Ingrese la nota promedido del trabajo en clase y extra-clase (/2.0): ");
    scanf("%f", &nota_TC_EC);
    getchar();
    printf("Ingrese la nota promedido de lecciones y participación en clase (/2.0): ");
    scanf("%f", &nota_LPC);
    getchar();
    printf("Ingrese la nota promedido del aprendizaje autónomo (/2.0): ");
    scanf("%f", &nota_AA);
    getchar();
    printf("Ingrese la nota promedido de evaluaciones (/4.0): ");
    scanf("%f", &nota_EV);
    getchar();

    notaFinal =  nota_TC_EC + nota_LPC + nota_AA + nota_EV;

    if (notaFinal > 10 || notaFinal < 0){
        printf("LOS VALORES NO CORRESPONDE AL A ESCALA DE LA NOTA \n");
        estado = "NO EXISTE";
    } else{
        if (notaFinal >= 7.0){
            estado = "APROBADO";
        } else{
            if (notaFinal >= 2.40){
                estado = "EN SUSPENSO";
            } else {
                estado = "REPROBADO";
            }
        }
    }
    printf("Nota Final: %.2f ---> %s\n", notaFinal, estado);

    printf("Teclee alguna tecla para salir...");
    getchar();
    return 0;
}
