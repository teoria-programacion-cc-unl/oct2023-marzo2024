/**
 * @file EscalaAcreditacion1.c
 * @author wduck
 * @brief 
 * @version 0.1
 * @date 2023-12-05
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include <stdio.h>
#include <stdlib.h>

struct Acreditacion{
    float trabajos;
    float lecciones;
    float aprendizajeAutonomo;
    float notaFinal;
};

int main(int argc, char const *argv[]){
    system("@cls||clear");
    
    struct Acreditacion acreditacion;    
    char* estado;

    printf("Ingrese la nota promedido del trabajo en clase y extra-clase (/2.0): ");
    scanf("%f", &nota_TC_EC);
    getchar();
    printf("Ingrese la nota promedido de lecciones y participación en clase (/2.0): ");
    scanf("%f", &nota_LPC);
    getchar();
    printf("Ingrese la nota promedido del aprendizaje autónomo (/2.0): ");
    scanf("%f", &nota_AA);
    getchar();
    printf("Ingrese la nota promedido de evaluaciones (/4.0): ");
    scanf("%f", &nota_EV);
    getchar();

    notaFinal =  nota_TC_EC + nota_LPC + nota_AA + nota_EV;

    if (notaFinal > 10 || notaFinal < 0){
        printf ("Los valores no corresponde a la escala de calificación\n");
        estado = "NO EXISTE";
        //sprintf(estado, "NO EXISTE");
    } else{
        if (notaFinal >= 7.0){
            estado = "APROBADO";
        } else{
            if (notaFinal >= 2.40){
                estado = "EN SUSPENSO";
            } else{
                estado = "REPROBADO";
            }
        }
    }
    printf("Nota final: %.2f ---> %s\n", notaFinal, estado);

    printf("Teclee alguna tecla para salir...");
    getchar();
}
    return 0;
