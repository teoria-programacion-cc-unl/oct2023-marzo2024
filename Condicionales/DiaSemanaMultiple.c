/**
 * @file DiaSemanaMultiple.c
 * @author wduck
 * @brief 
 * @version 0.1
 * @date 2023-12-05
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include <stdio.h>
#include <stdlib.h>

enum SEMANA {
    LUNES = 1, MARTES, MIERCOLES, JUEVES, VIERNES, SABADO, DOMINGO
};

enum BOOLEAN {
    FALSO, TRUE
};

int main(int argc, char const *argv[]){
    
    system("@cls||clear");

    enum SEMANA numDia;
    //int numDia;
    printf ("Escribir ingrese el número de día: ");
    //scanf("%i",&numDia);
    scanf("%u",&numDia);
    getchar();

    switch (numDia){
        case LUNES:
            printf("%d --> LUNES\n", numDia);
            break;
        case MARTES: {
            printf("%d --> MARTES\n", numDia);
            break;
        }
        case MIERCOLES: {
            printf("%d --> MIERCOLES\n", numDia);
            break;
        }
        case JUEVES: {
            printf("%d --> JUEVES\n", numDia);
            break;
        }
        case VIERNES: {
            printf("%d --> VIERNES\n", numDia);
            break;
        }
        case SABADO: {
            printf("%d --> SABADO\n", numDia);
            break;
        }
        case DOMINGO: {
            printf("%d --> DOMINGO\n", numDia);
            break;
        }
        default:
            printf("%d --> ESTE DÍA NO EXISTE EN MI MUNDO\n", numDia);
            break;
    }
   
    return 0;
}
