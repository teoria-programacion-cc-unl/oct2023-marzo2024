/**
 * @file EdadExacta-B.c
 * @author wduck
 * @brief Edad exacta de una persona
 * @version 0.1
 * @date 2023-12-07
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int main(int argc, char const *argv[]){
    system("@cls||clear");

    struct tm fechaNacimiento;
    struct tm fechaActual;
    struct tm edadExacta;

    int prestamoMes, prestamoAnio;

    printf("Ingrese la fecha de nacimiento (yyyy-mm-dd): ");
    scanf("%d-%d-%d", &fechaNacimiento.tm_year, &fechaNacimiento.tm_mon, &fechaNacimiento.tm_mday);
    getchar();

    // Inicio obtenerFechaActual()
    time_t segundosActuales = time(NULL);
    fechaActual = *localtime(&segundosActuales);
    fechaActual.tm_year = fechaActual.tm_year + 1900;
    fechaActual.tm_mon = fechaActual.tm_mon + 1;
    // Fin obtenerFechaActual()

    prestamoMes = 0;
    prestamoAnio = 0;

    edadExacta.tm_mday = fechaActual.tm_mday - fechaNacimiento.tm_mday;
    if (edadExacta.tm_mday <0) {
        prestamoMes = 1;
        edadExacta.tm_mday = edadExacta.tm_mday + 30;
    }

    edadExacta.tm_mon = (fechaActual.tm_mon - prestamoMes) - fechaNacimiento.tm_mon;
    if (edadExacta.tm_mon < 0){
        prestamoAnio = 1;
        edadExacta.tm_mon = edadExacta.tm_mon + 12;
    }

    edadExacta.tm_year = (fechaActual.tm_year - prestamoAnio) - fechaNacimiento.tm_year;
    if (edadExacta.tm_year >= 0){
        printf("Su edad es: %d anio(s), %d mes(es), %d día(s)\n", edadExacta.tm_year, edadExacta.tm_mon, edadExacta.tm_mday);
    } else {
        printf("Ud. aún no ha nacido \n");
    }
    printf("Para la fecha actual: %d-%d-%d\n", fechaActual.tm_year, fechaActual.tm_mon, fechaActual.tm_mday);
    printf("Gracias por usar nuestro programa!!\n");
    getchar();
    return 0;
}
