/**
 * @file PromocionColorBolita.c
 * @author wduck
 * @brief Calcular el pago total a través del sorteo del color de la bolita
 * @version 0.1
 * @date 2023-12-07
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define LETRA_COLOR_VERDE "\033[32m"
#define LETRA_COLOR_AMARILLO "\033[93m"
#define LETRA_COLOR_AZUL "\033[34m"
#define LETRA_COLOR_ROJO "\033[31m"
#define LETRA_COLOR_BLANCO "\033[97m"
#define LETRA_COLOR_GRIS "\033[37m"
#define FONDO_COLOR_NEGRO "\033[40m"

#define COLOR_POR_DEFECTO "\033[0m"
#define LETRA_NEGRITA "\033[1m"

struct ResumenComprobante{
    float subtotal;
    int porcentajeDscto;
    float valorDscto;
    float total;
};

enum ANFORA{
    BLANCO, VERDE, AMARILLO, AZUL, ROJO
};

int main(int argc, char const *argv[]){
    system("@cls||clear");

    struct ResumenComprobante resumenComprobante;
    enum ANFORA colorBolita;
    
    printf("%s%s", LETRA_COLOR_AMARILLO, LETRA_NEGRITA);
    printf("BIENVENIDOS A LA TIENDA XYZ\n");
    printf("=========================================\n");
    printf("%s", COLOR_POR_DEFECTO);
    printf("Este programa te permite sabel el descuento obtenido y el total a pagar\n\n");
    printf("%s", LETRA_COLOR_GRIS);
    printf("Ingrese el total de su compra: ");
    scanf("%f", &resumenComprobante.subtotal);
    getchar();

    printf("Sorteando.... \n");

    // Inicio Sortear la Bolita
    /* Intializes random number generator */
    srand((unsigned)time(NULL)); // Mezclar
    colorBolita = rand() % 4; // Obtener   
    // Inicio Sortear la Bolita

    printf("Resultado del sorteo: bolita ");
    switch (colorBolita){
        case VERDE:
            printf("%s%s", LETRA_COLOR_VERDE, LETRA_NEGRITA);
            printf("VERDE\n");
            resumenComprobante.porcentajeDscto = 10;
            break;
        case AMARILLO:
            printf("%s%s", LETRA_COLOR_AMARILLO, LETRA_NEGRITA);
            printf("AMARILLA\n");
            resumenComprobante.porcentajeDscto = 25;
            break;   
        case AZUL:
            printf("%s%s", LETRA_COLOR_AZUL, LETRA_NEGRITA);
            printf("AZUL\n");
            resumenComprobante.porcentajeDscto = 50;
            break;
        case ROJO:
            printf("%s%s", LETRA_COLOR_ROJO, LETRA_NEGRITA);
            printf("ROJA\n");
            resumenComprobante.porcentajeDscto = 100;
            break;
        default:
            printf("%s%s%s", LETRA_COLOR_BLANCO, FONDO_COLOR_NEGRO, LETRA_NEGRITA);
            printf("BLANCA\n");          
            resumenComprobante.porcentajeDscto = 0;
            break;
    }
    printf("%s", COLOR_POR_DEFECTO);

    resumenComprobante.valorDscto = resumenComprobante.subtotal * resumenComprobante.porcentajeDscto / 100;
    resumenComprobante.total = resumenComprobante.subtotal - resumenComprobante.valorDscto;

    printf("%s%s", LETRA_COLOR_GRIS, LETRA_NEGRITA);
    printf("RESUMEN DEL COMPROBANTE\n");
    printf("==========================================\n");
    printf("Subtotal\t\t\t: %.2f\n", resumenComprobante.subtotal);
    printf("Descuento (%d %%)\t\t: %.2f\n", resumenComprobante.porcentajeDscto, resumenComprobante.valorDscto);
    printf("-------------------------------------------\n");
    printf("%s%s", LETRA_COLOR_VERDE, LETRA_NEGRITA);
    printf("TOTAL\t\t\t: %.2f\n\n", resumenComprobante.total);

    printf("%s%s", LETRA_COLOR_GRIS, COLOR_POR_DEFECTO);
    printf("Gracias por usar nuestro programa!!\n");
    getchar();

    return 0;
}
