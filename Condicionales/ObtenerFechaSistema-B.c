/**
 * @file ObtenerFechaSistema-B.c
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2023-12-07
 * 
 * @copyright Copyright (c) 2023
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char const *argv[]){
    system("@cls||clear");
    // Obtener la fecha del sistema (computador) en segundos desde 1970
    time_t timeSeconds = time(NULL);
    printf("Los segundos desde 1970 son: %ld\n", timeSeconds);
    // Transaforma los segundos a tipo de dato Fecha-tiempo
    struct tm *currentDate = localtime(&timeSeconds);

    //currentDate->tm_year = currentDate->tm_year + 1900;
    //currentDate->tm_mon = currentDate->tm_mon + 1;
    printf("Year: %d\n", currentDate->tm_year+1900);
    printf("Year de acuerdo tm: %d\n", currentDate->tm_year);
    printf("Mes: %d\n", currentDate->tm_mon+1);
    printf("Mes de acuerdo: %d\n", currentDate->tm_mon);
    printf("Día: %d\n", currentDate->tm_mday);
    printf("Hora: %d\n", currentDate->tm_hour);
    printf("Minutos: %d\n", currentDate->tm_min);
    printf("Segundos: %d\n", currentDate->tm_sec);

    char *currentDateTexto1 = asctime(currentDate);
    printf("Actual Fecha y tiempo (STR): %s\n", currentDateTexto1);

    char currentDateTexto[50];
    strftime(currentDateTexto, 50, "%Y-%m-%d %X", currentDate);

    printf("Actual Fecha y tiempo (STR) con reglas de formato: %s\n", currentDateTexto);
    printf("Actual Fecha y tiempo manual %d-%d-%d, %d:%d:%d\n", currentDate->tm_year+1900, currentDate->tm_mon+1, currentDate->tm_mday, currentDate->tm_hour, currentDate->tm_min, currentDate->tm_sec);

    return 0;
}
