/**
 * @file ObtenerFechaSistema.c
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2023-12-06
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char const *argv[]){
    //system("@cls||clear");
    // Obtener el tiempo del sistema (computador) en segundos desde 1970
    time_t nowSeconds = time(NULL);
    printf("Los segundos desde 1970 son: %ld\n", nowSeconds);
    struct tm *currentDate = localtime(&nowSeconds);

    currentDate->tm_year = currentDate->tm_year + 1900;
    currentDate->tm_mon = currentDate->tm_mon + 1;

    printf("Información de estruct tm: \n");
    //printf("Year: %i\n", (currentDate->tm_year+1900));
    printf("Year: %i\n", currentDate->tm_year);
    //printf("Mes: %i\n", currentDate->tm_mon+1);
    printf("Mes: %i\n", currentDate->tm_mon);
    printf("Dia: %i\n", currentDate->tm_mday); 

    printf("Tiempo: %d:%d:%d\n", currentDate->tm_hour, currentDate->tm_min, currentDate->tm_sec);

    printf("Current local time and date: %s", asctime(currentDate));
    char fechaTexto[80];
    strftime(fechaTexto, 80, "%Y-%m-%d - %X", currentDate);
    printf("\n\nFecha formateada: %s\n", fechaTexto);


    return 0;
}
