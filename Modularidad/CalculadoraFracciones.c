/**
 * @file CalculadoraFracciones.c
 * @author wduck
 * @brief Realizar una calculadora de fracciones
 * @version 0.1
 * @date 2024-01-30
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#include <stdio.h>
#include <stdlib.h>

#define MAX_ELEMENTOS 100

struct Fraccion{
    int numerador, denominador;
};

struct CalculadoraFracciones{
   struct Fraccion fraccion1;
   struct Fraccion fraccion2;
};


struct Fraccion sumar(struct Fraccion fraccion1, struct Fraccion fraccion2);
struct Fraccion restar(struct Fraccion fraccion1, struct Fraccion fraccion2);
struct Fraccion multiplicar(struct Fraccion fraccion1, struct Fraccion fraccion2);
struct Fraccion dividir(struct Fraccion fraccion1, struct Fraccion fraccion2);
struct Fraccion simplificar(struct Fraccion fraccion);
void simplificarPorReferencia(struct Fraccion *fraccion);
float convertirPuntoFlotante(struct Fraccion fraccion);
struct Fraccion ingresarFraccion(char *mensaje);
char elegirOperacion();
struct Fraccion calcular(char operacion, struct CalculadoraFracciones calculadora);

void presentarFraccion(struct Fraccion fraccion);
void presentarResultado(char operacion, struct CalculadoraFracciones calculadora, struct Fraccion rta);

int main(int argc, char const *argv[])
{
    system("@cls||clear");
    struct Fraccion fraccion1 = ingresarFraccion("Ingrese fraccion 1: ");
    struct Fraccion fraccion2 = ingresarFraccion("Ingrese fraccion 2: ");

    struct CalculadoraFracciones calculadora = {fraccion1, fraccion2};
    char opcion = elegirOperacion();
    struct Fraccion rta = calcular(opcion, calculadora);

    presentarResultado(opcion, calculadora, rta);
    
    return 0;
}

void presentarResultado(char operacion, struct CalculadoraFracciones calculadora, struct Fraccion rta){
    printf("\n");
    presentarFraccion(calculadora.fraccion1);
    printf(" %c ", operacion);
    presentarFraccion(calculadora.fraccion2);
    printf(" = ");
    presentarFraccion(rta);
    printf(" ===> ");
    printf("%f \n", convertirPuntoFlotante(rta));
}

struct Fraccion calcular(char operacion, struct CalculadoraFracciones calculadora){
    struct Fraccion rta;
    switch (operacion){
        case '+':
            rta = sumar(calculadora.fraccion1,calculadora.fraccion2);
            break;
        case '-':
            rta = restar(calculadora.fraccion1,calculadora.fraccion2);
            break;
        case '*':
            rta = multiplicar(calculadora.fraccion1,calculadora.fraccion2);
            break;
        case '/':
            rta = dividir(calculadora.fraccion1,calculadora.fraccion2);
            break;
        default:
            rta.numerador = 0;
            rta.denominador = 0;
            break;
    }   
    return rta;
}


struct Fraccion ingresarFraccion(char *mensaje){
    struct Fraccion fraccion;
    //printf("%s[forma # / #]: " ,mensaje);
    //scanf("%d/%d", &fraccion.numerador, &fraccion.denominador);
    printf("%s:\n", mensaje);
    printf("Numerador: ");
    scanf("%d", &fraccion.numerador);
    getchar();
    printf("Denominador: ");
    scanf("%d", &fraccion.denominador);
    getchar();
    return fraccion; 
}



struct Fraccion simplificar(struct Fraccion fraccion){
    if (fraccion.denominador == 0){
        fraccion.numerador = 0;
        return fraccion;
    }

    int menor = fraccion.numerador;
    if (menor > fraccion.denominador){
        menor = fraccion.denominador;
    }

    for (int i = menor; i > 1; i--){
        if (fraccion.numerador % i == 0 &&  fraccion.denominador % i == 0){
            fraccion.numerador  = fraccion.numerador / i;
            fraccion.denominador  = fraccion.denominador / i;
            break;
        }
    }
    return fraccion;
}

void simplificarPorReferencia(struct Fraccion *fraccion){
    struct Fraccion simplificada = simplificar(*fraccion);
    fraccion->numerador = simplificada.numerador;
    fraccion->denominador = simplificada.denominador;
}

float convertirPuntoFlotante(struct Fraccion fraccion){
    return (float)fraccion.numerador / (float)fraccion.denominador;
}

void presentarFraccion(struct Fraccion fraccion){
    printf("%d / %d", fraccion.numerador, fraccion.denominador);
}